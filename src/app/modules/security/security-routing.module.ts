import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SecuirtyHomeComponent} from './secuirty-home/secuirty-home.component';
import { Routes, RouterModule } from '@angular/router';
const securityRoutes: Routes = [
  {
    path: 'tabs',
    component: SecuirtyHomeComponent,
    children:
      [
        {
          path: 'tab1',
          children:
            [
              {
                path: '',
                loadChildren: './visitors/visitors.module#VisitorsPageModule'
              }
            ]
        },
        {
          path: 'tab2',
          children:
          [
            {
              path: '',
              loadChildren: './parcel-pickup/parcel-pickup.module#ParcelPickupPageModule'
            }
          ]
        },
        {
          path: 'tab3',
          children:
          [
            {
              path: '',
              loadChildren: './kids-exit/kids-exit.module#KidsExitPageModule'
            }
          ]
        },
        {
          path: 'tab4',
          children:
          [
            {
              path: '',
              loadChildren: './check-out/check-out.module#CheckOutPageModule'
            }
          ]
        },
  
  
        {
          path: '',
          redirectTo: '/security/tabs/tab1',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '',
    redirectTo: '/security/tabs/tab1',
    pathMatch: 'full'
  }
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(securityRoutes)
  ]
})
export class SecurityRoutingModule { }
