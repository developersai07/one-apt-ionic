import { Component, OnInit,Input } from '@angular/core';
import { NavController, NavParams,ModalController } from '@ionic/angular';
import {VendorCheckinService} from '../../services/vendor-checkin.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { __core_private_testing_placeholder__ } from '@angular/core/testing';
@Component({
  selector: 'app-vendor-checkin',
  templateUrl: './vendor-checkin.component.html',
  styleUrls: ['./vendor-checkin.component.scss'],
})
export class VendorCheckinComponent implements OnInit {

  @Input() vendor: any;
  public vendorObj : any;
  public selectedArea = 'Individual';
  public indServicesArray: any;
  public comServicesArray: any;
  public childEntites: any = [];
  public grandChildEntites : any = [];
  public vendorprofileImage : any;
  public role : any;
  public visitorprofileImage: any;

  constructor(navParams: NavParams,public toastController: ToastController,public modalctrl : ModalController,public  vservice: VendorCheckinService,public httpservice: HttpCallsService,) {
    this.vendorObj  = navParams.get('vendor');
    console.log(this.vendorObj)
    this.role  = navParams.get('role');
    console.log(  this.role)
    this.vservice.form.controls['serArea'].setValue('Individual');
 
   }

  ngOnInit() {
    this.loadIndServices();
this.getAllComService();
this.loadAppartmentChildEntites();
  }

  loadAppartmentChildEntites(){
    this.httpservice.getHttpPost('appartmentChildEntities/getByAppartment/' + localStorage.getItem("appartmentId") + '').subscribe((data: any) => {
      console.log(data)
      this.childEntites = data;
      this.childEntites.length;

      console.log(  this.childEntites.length)
   
    });
  }

  loadIndServices(){
    this.httpservice.getHttpPost('indService/allAppIndService/' + localStorage.getItem("associationId") + '').subscribe((data: any) => {
      console.log(data)
      if (data.length > 0) {
        console.log(data.length);
        this.indServicesArray = data;
      }
    });

  }

  onVisitorPicChange(event){
    let files = event.target.files;
    this.visitorprofileImage = files[0];
  }

  getAllComService() {
    this.httpservice.getHttpPost('comService/allAppComService/' + localStorage.getItem("associationId") + '').subscribe((data: any) => {
      if (data.length > 0) {

        this.comServicesArray = data;
      }
    });

  }


  onSubmit(data){
    let obj : any;
    var formData = new FormData();
if(data.value.serArea == 'Individual'){


  formData.append("serArea", data.value.serArea)
  formData.append("block", data.value.block)
  formData.append("flats", data.value.flats)
  formData.append("indService", data.value.indService)
  formData.append("persons", data.value.persons)
  formData.append("vv_pic", this.vendorprofileImage);
  formData.append("associationId",  localStorage.getItem("associationId"));
  formData.append("vendorId",   this.vendorObj.id);
 
  formData.append("created_by", localStorage.getItem("appUserId"));
}
else {
  
  formData.append("serArea", data.value.serArea)
  formData.append("comService", data.value.comService)
  formData.append("persons", data.value.persons)
  formData.append("vv_pic", this.vendorprofileImage)
  formData.append("created_by", localStorage.getItem("appUserId"));
  formData.append("associationId",  localStorage.getItem("associationId"));
}
this.httpservice.addHttpPost('visitor/vendor/checkIn',formData).subscribe((data: any) => {
  console.log(data)
  if (data) {
    this.presentToast("Vendor Checked In");
    window.location.reload();
  }

});
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  onVisitorSubmit(data){
    var formData = new FormData();
    console.log(data.value);
    formData.append("visitorId",   this.vendorObj.ID);
    formData.append("visitor_pic",   this.visitorprofileImage);
    this.httpservice.addHttpPost('visitor/visitorCheckIn',formData).subscribe((data: any) => {
      console.log(data)
      if (data) {
        this.presentToast("Visitor Checked In");
        //window.location.reload();
        setTimeout(() => {
          window.location.reload();
      }, 3000);
      }
    
    });

  }

  onVendorPicChange(event){
    let files = event.target.files;
    console.log(files);
   // this.imgFilename = files[0].name;
    this.vendorprofileImage = files[0];
  }

  onChangeArea($event){
    console.log($event.target.value);
    if($event.target.value == 'Individual'){
console.log("indivual Selected")
this.vservice.form.addControl('indService', new FormControl('', Validators.required));
this.vservice.form.addControl('flats', new FormControl('', Validators.required));
this.vservice.form.addControl('block', new FormControl('', Validators.required));
    }
    else {
      this.vservice.form.addControl('comService', new FormControl('', Validators.required));
      this.vservice.form.removeControl('indService');
      this.vservice.form.removeControl('flats');
      this.vservice.form.removeControl('block');
      console.log("Common Selected")
    }
  }

  onChangeBlock($event){
    console.log($event.target.value);
    this.loadGrandChild($event.target.value);

    console.log("caling")

  }
  loadGrandChild(id){
    this.httpservice.getHttpPost('appartmentGrandChildEntities/getGrandChild/' + id + '').subscribe((data: any) => {
      console.log(data)
      this.grandChildEntites = data;
   
    });
  }

  
  //function for close modal of book toy
  modal_close()
  {

    this.modalctrl.dismiss();

  }


}
