import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SecurityRoutingModule} from './security-routing.module'

import {SecuirtyHomeComponent} from './secuirty-home/secuirty-home.component';
import {SuperTabsModule} from '@ionic-super-tabs/angular';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { Routes, RouterModule } from '@angular/router';

import {VendorCheckinComponent} from './modals/vendor-checkin/vendor-checkin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SecuirtyHomeComponent,VendorCheckinComponent],
  imports: [
    CommonModule,
    SecurityRoutingModule,
    SuperTabsModule,
    IonicModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [SecuirtyHomeComponent,VendorCheckinComponent ]
})
export class SecurityModule { }
