import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-kids-exit',
  templateUrl: './kids-exit.page.html',
  styleUrls: ['./kids-exit.page.scss'],
})
export class KidsExitPage implements OnInit {

  public kidsExitList: any;

  constructor(public httpCallsService: HttpCallsService, public router: Router, public toastController: ToastController) { }

  ngOnInit() {
    this.getKidsExitPermitData();
  }

  getKidsExitPermitData() {
    this.httpCallsService.getHttpPost('kidsExit/getKidsExitPermitdata/' + localStorage.getItem('associationId') + '')
      .subscribe((data: any) => {
        this.kidsExitList = data;
        console.log(this.kidsExitList);

      });
  }

  approve(kidsExitId) {
    console.log(kidsExitId);
    const obj = {
      id: kidsExitId,
      securityId: localStorage.getItem('securityId'),
      updated_by: localStorage.getItem('appUserId')
    };
    console.log(obj);
    this.httpCallsService.addHttpPost('kidsExit/updateSecurityKidsExitApproved', obj).subscribe((data: any) => {
      console.log(data);
      if (data.ID && data.IS_EXIT == true) {
        this.presentToast('Kids Exit Approved Sucessfully');
        setTimeout(() => {
          window.location.reload();
        }, 3000);
      } else {
        this.presentToast('Kids Exit Status Not Updated');
      }
    });
  }

  // rejected(kidsExitId) { 
  //   console.log(kidsExitId);
  //   const obj = {
  //     securityId: localStorage.getItem('securityId')
  //   };
  //   this.httpCallsService.addHttpPost('kidsExit/updateSecurityKidsExitRejected', obj).subscribe((data: any) => {
  //     console.log(data);
  //     if (data.ID && data.IS_EXIT == false) {
  //       this.presentToast('Kids Exit Rejected Sucessfully');
  //       setTimeout(() => {
  //         window.location.reload();
  //       }, 3000);
  //     } else {
  //       this.presentToast('Kids Exit Status Not Updated ');
  //     }
  //   });
  //  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
