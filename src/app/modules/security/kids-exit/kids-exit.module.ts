import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KidsExitPageRoutingModule } from './kids-exit-routing.module';

import { KidsExitPage } from './kids-exit.page';
import {SharedModuleModule} from '../shared-module/shared-module.module'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KidsExitPageRoutingModule,
    SharedModuleModule
  ],
  declarations: [KidsExitPage]
})
export class KidsExitPageModule {}
