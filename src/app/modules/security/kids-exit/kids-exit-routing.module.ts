import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KidsExitPage } from './kids-exit.page';

const routes: Routes = [
  {
    path: '',
    component: KidsExitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KidsExitPageRoutingModule {}
