import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KidsExitPage } from './kids-exit.page';

describe('KidsExitPage', () => {
  let component: KidsExitPage;
  let fixture: ComponentFixture<KidsExitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsExitPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KidsExitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
