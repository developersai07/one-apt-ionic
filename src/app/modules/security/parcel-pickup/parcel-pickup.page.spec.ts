import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParcelPickupPage } from './parcel-pickup.page';

describe('ParcelPickupPage', () => {
  let component: ParcelPickupPage;
  let fixture: ComponentFixture<ParcelPickupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParcelPickupPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParcelPickupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
