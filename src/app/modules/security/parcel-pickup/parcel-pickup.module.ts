import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParcelPickupPageRoutingModule } from './parcel-pickup-routing.module';

import { ParcelPickupPage } from './parcel-pickup.page';
import {SharedModuleModule} from '../shared-module/shared-module.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParcelPickupPageRoutingModule,
    SharedModuleModule
  ],
  declarations: [ParcelPickupPage]
})
export class ParcelPickupPageModule {}
