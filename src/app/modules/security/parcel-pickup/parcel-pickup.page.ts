import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { ToastController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-parcel-pickup',
  templateUrl: './parcel-pickup.page.html',
  styleUrls: ['./parcel-pickup.page.scss'],
})
export class ParcelPickupPage implements OnInit {

  segment: string;
  pickupRequestData: any;
  handoverData: any;

  // tslint:disable-next-line: max-line-length
  constructor(public httpCallsService: HttpCallsService, public alertController: AlertController, public toastController: ToastController, ) { }

  ngOnInit() {
    this.getPickupRequestData();
    this.getHandoverData();
    this.segment = 'Pickup';
  }

  checkHandover(value) {
    console.log(value);
    const obj = {
      uniqueNumber: value,
      assId: localStorage.getItem('associationId'),
      appUserId: localStorage.getItem('appUserId'),
      updated_by: localStorage.getItem('appUserId'),
    };
    console.log(obj);
    this.httpCallsService.addHttpPost('parcelPickUp/updatCheckHanddover', obj).subscribe((data: any) => {
      console.log(data);
      if (data.ID) {
        this.presentToast('Pickup Parcel Handovered Succesfully');
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }  else {
        console.log(data);
        this.presentToast('Pickup Parcel Not Handovered');
        // this.ngOnInit();
      }
    })
    // this.segment = 'Handover';
  }

  getPickupRequestData() {
    console.log('pickup');
    const associationId = localStorage.getItem('associationId');
    this.httpCallsService.getHttpPost('parcelPickup/getPickupRequestData/' + associationId + '').subscribe((data: any) => {
      console.log(data);
      this.pickupRequestData = data;
    });
  }

  getHandoverData() {
    console.log('handover');
    const associationId = localStorage.getItem('associationId');
    this.httpCallsService.getHttpPost('parcelPickup/getHandoverData/' + associationId + '').subscribe((data: any) => {
      console.log(data);
      this.handoverData = data;
    });
  }

  pickedUp(requestid) {
    const obj = {
      id: requestid,
      appUserId: localStorage.getItem('appUserId'),
      updated_by: localStorage.getItem('appUserId')
    };
    console.log(obj);
    this.httpCallsService.addHttpPost('parcelPickup/updatePickUpData', obj).subscribe((data: any) => {
      console.log(data);
      if (data.ID) {
        this.presentToast('Parcel Pickup Sucessfully');
        setTimeout(() => {
          window.location.reload();
        }, 3000);
      } else {
        this.presentToast('Parcel Pickup Not Updated');
        this.ngOnInit();
      }
    });
  }

  async handOver(requestId) {
    console.log(requestId);
    const alert = await this.alertController.create({
      header: 'Handove Parcel !',
      inputs: [
        // multiline input.
        {
          name: 'UniqueNumber',
          id: 'uniqueNumer',
          type: 'text',
          placeholder: 'Unique Number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok');
            // console.log(data.Description);
            const obj = {
              id: requestId,
              un: data.UniqueNumber,
              // assId: localStorage.getItem('associationId'),
              appUserId: localStorage.getItem('appUserId'),
              updated_by: localStorage.getItem('appUserId'),
            };
            console.log(obj);
            this.httpCallsService.addHttpPost('parcelPickup/updateHandoverData', obj).subscribe((response: any) => {
              console.log(response);
              if (response.ID) {
                this.presentToast('Pickup Parcel Handovered Succesfully');
                setTimeout(() => {
                  window.location.reload();
                }, 3000);
              } else if (response.message === 'Check your unique number') {
                this.presentToast('Check your unique number');
                // this.ngOnInit();
              } else {
                this.presentToast('Pickup Parcel Not Handovered');
                this.ngOnInit();
              }
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async controller(requestId) {
    const alert = await this.alertController.create({
      header: 'Handove Parcel !',
      inputs: [
        // multiline input.
        {
          name: 'UniqueNumber',
          id: 'uniqueNumer',
          type: 'text',
          placeholder: 'Unique Number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok');
            // console.log(data.Description);
            const obj = {
              id: requestId,
              un: data.UniqueNumber,
              // assId: localStorage.getItem('associationId'),
              appUserId: localStorage.getItem('appUserId'),
              updated_by: localStorage.getItem('appUserId'),
            };
            console.log(obj);
            this.httpCallsService.addHttpPost('parcelPickup/updateHandoverData', obj).subscribe((response: any) => {
              console.log(response);
              if (response.ID) {
                this.presentToast('Pickup Parcel Handovered Succesfully');
                setTimeout(() => {
                  window.location.reload();
                }, 3000);
              } else if (response.message === 'Check your unique number') {
                this.presentToast('Check your unique number');
                // this.ngOnInit();
              } else {
                this.presentToast('Pickup Parcel Not Handovered');
                this.ngOnInit();
              }
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
