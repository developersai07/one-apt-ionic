import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParcelPickupPage } from './parcel-pickup.page';

const routes: Routes = [
  {
    path: '',
    component: ParcelPickupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParcelPickupPageRoutingModule {}
