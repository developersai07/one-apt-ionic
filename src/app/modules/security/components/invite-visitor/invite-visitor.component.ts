import { Component, OnInit } from '@angular/core';
import { NavController, NavParams,ModalController } from '@ionic/angular';
import {OwnerVisitorService} from '../../../Owner-Visitors/owner-visitor.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-invite-visitor',
  templateUrl: './invite-visitor.component.html',
  styleUrls: ['./invite-visitor.component.scss'],
})
export class InviteVisitorComponent implements OnInit {
  public visitorTypes: any;
  public ChildEntities: any;
  public grandChildEntities: any = [];
  public visitorprofileImage: any;

  constructor(public modalctrl : ModalController,public oWService : OwnerVisitorService,
    public toastController: ToastController,public httpservice: HttpCallsService,) { }

  ngOnInit() {
    this.loadVisitorTypes();
    this.loadChildEntities()
  }
  loadChildEntities(){
    this.httpservice.getHttpPost('appartmentChildEntities/getByAppartment/'+localStorage.getItem("associationId")+'').subscribe((data: any) => {
      console.log(data)
      this.ChildEntities = data
          })
  }
  loadVisitorTypes(){
    this.httpservice.getHttpPost('visitorTypes/getAll').subscribe((data: any) => {
console.log(data)
this.visitorTypes = data
    })
  }

  onVisitorPicChange(event){
    let files = event.target.files;
    this.visitorprofileImage = files[0];
  }

  save(data){
console.log(data.value)

const formData = new FormData();
formData.append("visitorName",data.value.visitorName);
formData.append("visitorType",data.value.visitorType);
formData.append("mobileNumber",data.value.mobileNumber);
formData.append("companyName",data.value.companyName);
formData.append("count",data.value.count);
formData.append("childEntites",data.value.childEntites);
formData.append("grandChildEntites",data.value.grandChildEntites);
formData.append("visitor_pic",this.visitorprofileImage);
formData.append("associationId",localStorage.getItem("associationId"));
formData.append("appUserId",localStorage.getItem("appUserId"));



this.httpservice.addHttpPost('visitor/SecurityvisitorRequest',formData).subscribe((data: any) => {
  console.log(data)
  //this.grandChildEntities = data
  if(data){
    this.presentToast("Visitor Requested Sucessfully");
    setTimeout(() => {
      window.location.reload();
  }, 3000);
  }
  else {
    this.presentToast("Unable to Request visitor");
  }
      })

  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }


  onChildEntity(data){
console.log(data.target.value)
this.httpservice.getHttpPost('appartmentGrandChildEntities/getGrandChild/'+data.target.value+'').subscribe((data: any) => {
  console.log(data)
  this.grandChildEntities = data
      })
  }
  modal_close()
  {

    this.modalctrl.dismiss();

  }


}
