import { Component, OnInit } from '@angular/core';
import { AlertController,ModalController  } from '@ionic/angular';
import {InviteVisitorComponent} from '../../components/invite-visitor/invite-visitor.component'
@Component({
  selector: 'app-secuirty-header',
  templateUrl: './secuirty-header.component.html',
  styleUrls: ['./secuirty-header.component.scss'],
})
export class SecuirtyHeaderComponent implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {}
  logOut(){
    localStorage.clear();
    window.location.reload();
  }

  async inviteVisitor(){
    const modal = await this.modalController.create({
      component: InviteVisitorComponent,
      // componentProps: {
      //   'vendor': data,
      //   'role' : role
    
      // }
    });
    return await modal.present();
  }

}
