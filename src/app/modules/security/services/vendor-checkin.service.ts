import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
@Injectable({
  providedIn: 'root'
})
export class VendorCheckinService {
  form: FormGroup = new FormGroup({
    serArea: new FormControl('', Validators.required),
    indService: new FormControl('', Validators.required),
   // comService: new FormControl('', Validators.required),
   flats: new FormControl('', Validators.required),
   block: new FormControl('', Validators.required),
    persons: new FormControl('', Validators.required),
    vendor_pic: new FormControl(''),
  })
  visitorform: FormGroup = new FormGroup({
    visitor_pic: new FormControl(''),
  })
  constructor() { }
}
