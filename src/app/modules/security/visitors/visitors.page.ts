import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from '../../../common/http-service.service';
import { AlertController, ModalController } from '@ionic/angular';
import { VendorCheckinComponent } from '../modals/vendor-checkin/vendor-checkin.component'
@Component({
  selector: 'app-visitors',
  templateUrl: './visitors.page.html',
  styleUrls: ['./visitors.page.scss'],
})
export class VisitorsPage implements OnInit {

  public isVisitorShow = true;
  public isVendorShow = false;
  something = 'visitor';
  public visitorWaitingList: any;
  constructor(public httpservice: HttpCallsService, public alertController: AlertController, public modalController: ModalController) { }

  ngOnInit() {
    console.log("visit Init ")
    this.loadWaitingList()
  }

  loadWaitingList() {
    this.httpservice.getHttpPost('visitor/visitorWaitingForApprovalList/' + localStorage.getItem("associationId") + '').subscribe((data: any) => {
      console.log(data);

      this.visitorWaitingList = data;

      //this.plansObj = data;
    });
  }



  ionViewWillEnter() {

    console.log("visit Init2")
    this.ngOnInit()
  }

  async presentVednorCheckInModal(data, role) {

    console.log(data)
    const modal = await this.modalController.create({
      component: VendorCheckinComponent,
      componentProps: {
        'vendor': data,
        'role': role

      }
    });
    return await modal.present();
  }


  onChangeHandler($event) {
    console.log($event.target.value);

    if ($event.target.value == 'visitor') {
      this.isVendorShow = false;
      this.isVisitorShow = true;
    }
    if ($event.target.value == 'vendor') {
      this.isVendorShow = true;
      this.isVisitorShow = false;
    }
  }

  async showAlert(message) {
    const alert = await this.alertController.create({
      header: 'Alert',
      //  subHeader: 'Subtitle',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  checkVisitor(value) {

    let data = {
      visitorCode: value
    }

    this.httpservice.addHttpPost('visitor/checkVisitorsExists', data).subscribe((data: any) => {
      console.log(data);

      if (data) {
        console.log("visitor Exists");
        this.presentVednorCheckInModal(data, 'visitor');
        //this.showAlert("Valid Vendor");
      }
      else {
        this.showAlert("In-Valid Visitor");
      }

      //this.plansObj = data;
    });
  }

  checkVendor(value) {
    console.log(value)
    let data = {
      visitorCode: value
    }

    this.httpservice.addHttpPost('vendor/checkVendorExists', data).subscribe((data: any) => {
      console.log(data);

      if (data) {
        this.presentVednorCheckInModal(data, 'vendor')
        //this.showAlert("Valid Vendor");
      }
      else {
        this.showAlert("In-Valid Vendor");
      }

      //this.plansObj = data;
    });
  }

}
