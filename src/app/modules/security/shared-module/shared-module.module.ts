import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SecuirtyHeaderComponent} from '../components/secuirty-header/secuirty-header.component';
import {InviteVisitorComponent} from '../components/invite-visitor/invite-visitor.component';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [SecuirtyHeaderComponent,InviteVisitorComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[SecuirtyHeaderComponent,InviteVisitorComponent],
  entryComponents : [InviteVisitorComponent]
})
export class SharedModuleModule { }
