import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from '../../../common/http-service.service';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.page.html',
  styleUrls: ['./check-out.page.scss'],
})
export class CheckOutPage  {
public vendorCheckedInList : any;
public visitorCheckedInList: any;
public segment = "vendors"
  constructor(public httpservice: HttpCallsService,public toastController: ToastController,) { 
    console.log("checkOut Loadinbg23")
  }

  ionViewWillEnter() {

    console.log("checkOut Loadinbg")
    this.checkinVendors();

    this.checkInVisitors()
  }

  checkInVisitors(){
    this.httpservice.getHttpPost('visitor/visitorCheckedInList/'+localStorage.getItem("associationId")+'').subscribe((data: any) => {
      console.log(data)
     this.visitorCheckedInList = data;
      
      
          })
  }

  checkinVendors(){

    this.httpservice.getHttpPost('visitor/vendor/vendorCheckedIn/'+localStorage.getItem("associationId")+'').subscribe((data: any) => {
console.log(data)
this.vendorCheckedInList = data;


    })
    
  }

  visitorCheckOut(id){
console.log(id)
this.httpservice.getHttpPost('visitor/visitorCheckOut/'+id+'').subscribe((data: any) => {
  console.log(data)
if(data){
this.presentToast("visitor Checked Out Sucessfully")
window.location.reload();
}
  
      })
  }

  vendorCheckOut(id){

    console.log(id)
    this.httpservice.getHttpPost('visitor/vendor/vendorCheckOut/'+id+'').subscribe((data: any) => {
      console.log(data)
  if(data){
    this.presentToast("visitor Checked Out Sucessfully")
window.location.reload();
  }
      
          })

  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }



}
