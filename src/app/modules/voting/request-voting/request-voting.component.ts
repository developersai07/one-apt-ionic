import { Component, OnInit } from '@angular/core';
import { RequestVotingService } from 'src/app/services/request-voting.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-request-voting',
  templateUrl: './request-voting.component.html',
  styleUrls: ['./request-voting.component.scss'],
})
export class RequestVotingComponent implements OnInit {

  public requestedVotingList: any;

  constructor(public requestVotingService: RequestVotingService, public httpCallsService: HttpCallsService,
    public alertController: AlertController, public toastCtrl: ToastController, public router: Router) {

  }

  ngOnInit() {
    this.votingRequest();
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  async onClick() {
    const alert = await this.alertController.create({
      header: 'Request Voting!',
      inputs: [
        // multiline input.
        {
          name: 'Description',
          id: 'description',
          type: 'textarea',
          placeholder: 'Request For Voting'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok');
            // console.log(data.Description);
            const obj = {
              description: data.Description,
              assId: localStorage.getItem('associationId'),
              appUserId: localStorage.getItem('appUserId')
            };
            console.log(obj);
            this.httpCallsService.addHttpPost('voting/addRequestVoting', obj).subscribe((response) => {
              console.log(response);
              this.presentToast('Voting Request Raised Succesfully');
              // this.router.navigate(['dashboard']);
              this.ngOnInit();
            });
          }
        }
      ]
    });

    await alert.present();
  }

  // onSubmit(value) {
  //   console.log(value);
  //   console.log(localStorage.getItem('associationId'));
  //   console.log(localStorage.getItem('appUserId'));
  //   const obj = {
  //     title: value.title,
  //     description: value.description,
  //     appUserId: localStorage.getItem('appUserId'),
  //     assId: localStorage.getItem('associationId'),
  //     flatOwnerId: localStorage.getItem('flatOwnerId'),
  //     created_by: localStorage.getItem('appUserId'),
  //   };
  //   console.log(obj);
  //   // this.httpservice.addHttpPost('voting/requestVoting', obj).subscribe((data) => {

  //   // });

  // }

  votingRequest() {
    this.httpCallsService.getHttpPost('voting/getVotingRequestByAssociation/' + localStorage.getItem('associationId') + '')
      .subscribe((data: any) => {
        console.log(data);
        this.requestedVotingList = data;
      });
  }

  getRequestVotingAction(data) {
    // let mapFlatOwnerArray = [];
    console.log(data);
    const mapFlatOwnerArray = data;
    const mapvoting = mapFlatOwnerArray.find(x => x.flatOwners.FLAT_OWNER_ID == localStorage.getItem('flatOwnerId'));
    console.log(mapvoting);
    if (mapvoting) {
      return true;
    } else {
      return false;
    }
  }

  async approve(ticketId) {

    // this.httpservice.getHttpPost('voting/mapFlatOwnerVotingAction/' + ticketId + '/' + localStorage.getItem('flatOwnerId') + '/' +
    //   localStorage.getItem('associationId') + '/1').subscribe((data: any) => {
    //     if (data) {
    //       this.presentToast('Voting Request Accepted Succesfully ');
    //       this.router.navigate(['dashboard']);
    //     }
    //   });

    const alert = await this.alertController.create({
      header: 'Comments!',
      inputs: [
        // multiline input.
        {
          name: 'Comments',
          id: 'coments',
          type: 'textarea',
          placeholder: 'Your Comments'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        },
        {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok');
            const dataObj = {
              comments: data.Comments
            };
            console.log(dataObj);

            this.httpCallsService.addHttpPost('voting/mapFlatOwnerVotingAction/' + ticketId + '/' + localStorage.getItem('flatOwnerId') + '/' +
              localStorage.getItem('associationId') + '/1', dataObj).subscribe((data: any) => {
                if (data) {
                  this.presentToast('Voting Request Accepted');
                  // this.router.navigate(['dashboard']);
                  this.ngOnInit();
                }
              });
          }
        }
      ]
    });

    await alert.present();
  }

  async reject(ticketId) {
    // tslint:disable-next-line: max-line-length
    // this.httpservice.getHttpPost('voting/mapFlatOwnerVotingAction/' + ticketId + '/' + localStorage.getItem('flatOwnerId') + '/' + localStorage.getItem('associationId') + '/2')
    //   .subscribe((data: any) => {
    //     if (data) {
    //       this.presentToast('Voting Request Rejected Succesfully ');
    //       this.router.navigate(['dashboard']);
    //     }
    //   });

    const alert = await this.alertController.create({
      header: 'Comments!',
      inputs: [
        // multiline input.
        {
          name: 'Comments',
          id: 'coments',
          type: 'textarea',
          placeholder: 'Your Comments'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        },
        {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok');
            const dataObj = {
              comments: data.Comments
            };
            console.log(dataObj);

            this.httpCallsService.addHttpPost('voting/mapFlatOwnerVotingAction/' + ticketId + '/' + localStorage.getItem('flatOwnerId') + '/' +
              localStorage.getItem('associationId') + '/2', dataObj).subscribe((data: any) => {
                if (data) {
                  this.presentToast('Voting Request Rejected');
                  // this.router.navigate(['dashboard']);
                  this.ngOnInit();
                }
              });
          }
        }
      ]
    });

    await alert.present();
  }

  getRequestVotingStatus(data) {
    const mapFlatOwnerArray = data;
    const mapvoting = mapFlatOwnerArray.find(x => x.flatOwners.FLAT_OWNER_ID == localStorage.getItem('flatOwnerId'));
    console.log(mapvoting);
    if (mapvoting) {
      if (mapvoting.IS_APPROVED == true) {
        return 'Approved';
      } else {
        return 'Rejected';
      }
    } else {
      return false;
    }
  }

  view(ticketId) {
    this.requestVotingService.setTicketId(ticketId);
    this.router.navigate(['voting/viewVoting']);
  }

}
