import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VotingRoutingModule } from './voting-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RequestVotingComponent } from './request-voting/request-voting.component';
import { ViewVotingComponent } from './view-voting/view-voting.component';
import { ChartsModule } from 'ng2-charts';



@NgModule({
  declarations: [RequestVotingComponent, ViewVotingComponent],
  imports: [
    CommonModule,
    VotingRoutingModule,
    FormsModule,
    ChartsModule,
    ReactiveFormsModule,
    IonicModule,
  ],
  entryComponents: [RequestVotingComponent, ViewVotingComponent]
})
export class VotingModule { }
