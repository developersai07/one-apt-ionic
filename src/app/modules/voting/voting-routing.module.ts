import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestVotingComponent } from './request-voting/request-voting.component';
import { ViewVotingComponent } from './view-voting/view-voting.component';


const votingRoutes: Routes = [
  { path: 'requstVoting', component: RequestVotingComponent , pathMatch: 'full'},
  { path: 'viewVoting', component: ViewVotingComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forChild(votingRoutes)],
  exports: [RouterModule]
})
export class VotingRoutingModule { }
