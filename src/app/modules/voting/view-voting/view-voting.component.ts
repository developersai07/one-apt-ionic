import { Component, OnInit } from '@angular/core';
import { RequestVotingService } from 'src/app/services/request-voting.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { AlertController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-voting',
  templateUrl: './view-voting.component.html',
  styleUrls: ['./view-voting.component.scss'],
})
export class ViewVotingComponent implements OnInit {

  doughnutChartLabels = ['Approved', 'Rejected'];
  doughnutChartData = [350, 450];
  doughnutChartType = 'doughnut';
  donutOptions: any = {
    legend: {
      display: true,
      position: 'right'
    }
  };

  segment: string;
  public ticketId: any;
  approvedCount: any;
  rejectedCount: any;
  votingData: any = [];

  constructor(public requestVotingService: RequestVotingService, public httpCallsService: HttpCallsService,
              public alertController: AlertController, public toastCtrl: ToastController, public router: Router) { }

  ngOnInit() {
    this.loadDataCount();
    this.loadDataInfo();
    this.segment = 'Approved';
  }

  loadDataCount() {
    this.ticketId = this.requestVotingService.getTicketId();
    console.log(this.ticketId);
    this.httpCallsService.getHttpPost('voting/getVotingResultCount/' + this.ticketId + '').subscribe((data: any) => {
      console.log('-------------');
      console.log(data);
      this.approvedCount = data[0];
      this.rejectedCount = data[1];
      this.doughnutChartData = data;
    });
  }

  loadDataInfo() {
    this.ticketId = this.requestVotingService.getTicketId();
    this.httpCallsService.getHttpPost('voting/getVotingInfo/' + this.ticketId + '').subscribe((data: any) => {
      console.log('-------------');
      console.log(data);
      this.votingData = data;
    });
  }

}
