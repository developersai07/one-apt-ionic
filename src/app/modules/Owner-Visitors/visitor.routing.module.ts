import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {VisirorAllowComponent} from './components/visiror-allow/visiror-allow.component';
import {VisirorInsideComponent} from './components/visiror-inside/visiror-inside.component';
import { Routes, RouterModule } from '@angular/router';
const securityRoutes: Routes = [
  {
    path: 'visitorRequest',
    component: VisirorAllowComponent,
    pathMatch: 'full'
  },
  {
    path: 'visitorInside',
    component: VisirorInsideComponent,
    pathMatch: 'full'
  }
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(securityRoutes)
  ]
})
export class VisitorRoutingModule { }
