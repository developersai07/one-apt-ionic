import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-visiror-inside',
  templateUrl: './visiror-inside.component.html',
  styleUrls: ['./visiror-inside.component.scss'],
})
export class VisirorInsideComponent implements OnInit {
  public visitorWaitingList: any =[];
  public segment= 'waiting';
  public visitorInsideList: any = []

  constructor(public httpservice: HttpCallsService,public toastController: ToastController) { }

  ngOnInit() {
    this.loadWaitingList();
    this.loadVisitoeInsideList()
  }

  loadWaitingList(){
    this.httpservice.getHttpPost('visitor/visitorWaitingForApprovalList/'+localStorage.getItem("associationId")+'',).subscribe((data: any) => {
      console.log(data);
    
    this.visitorWaitingList = data;
    
      //this.plansObj = data;
    });
  }

  loadVisitoeInsideList(){
    this.httpservice.getHttpPost('visitor/visitorInsideList/'+localStorage.getItem("appUserId")+'',).subscribe((data: any) => {
      console.log(data);
    
    this.visitorInsideList = data;
    
      //this.plansObj = data;
    });
  }

  

  visitorAllow(id){
    this.httpservice.getHttpPost('visitor/visitorAllow/'+id+'',).subscribe((data: any) => {
      console.log(data);
    if(data){
      this.presentToast("Ticket Allowed Sucessfully");
      setTimeout(() => {
        window.location.reload();
    }, 3000);
    }
    });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
  

}
