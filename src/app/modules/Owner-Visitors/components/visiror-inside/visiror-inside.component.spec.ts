import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisirorInsideComponent } from './visiror-inside.component';

describe('VisirorInsideComponent', () => {
  let component: VisirorInsideComponent;
  let fixture: ComponentFixture<VisirorInsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisirorInsideComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisirorInsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
