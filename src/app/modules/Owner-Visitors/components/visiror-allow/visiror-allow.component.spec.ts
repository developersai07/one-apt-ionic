import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VisirorAllowComponent } from './visiror-allow.component';

describe('VisirorAllowComponent', () => {
  let component: VisirorAllowComponent;
  let fixture: ComponentFixture<VisirorAllowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisirorAllowComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VisirorAllowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
