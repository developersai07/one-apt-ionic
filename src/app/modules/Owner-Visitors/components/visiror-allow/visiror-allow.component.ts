import { Component, OnInit } from '@angular/core';
import { OwnerVisitorService } from '../../owner-visitor.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-visiror-allow',
  templateUrl: './visiror-allow.component.html',
  styleUrls: ['./visiror-allow.component.scss'],
})
export class VisirorAllowComponent implements OnInit {
  public visitorTypes: any;

  constructor(public oWService: OwnerVisitorService, public httpservice: HttpCallsService, public toastController: ToastController, public router: Router) { }

  ngOnInit() {
    this.loadVisitorTypes()
  }

  loadVisitorTypes() {
    this.httpservice.getHttpPost('visitorTypes/getAll').subscribe((data: any) => {
      console.log(data)
      this.visitorTypes = data
    })
  }

  save(data) {
    console.log(data.value)
    let obj = {
      visitorName: data.value.visitorName,
      visitorType: data.value.visitorType,
      associationId: localStorage.getItem("associationId"),
      appUserId: localStorage.getItem("appUserId"),
      companyName: data.value.companyName,
      mobileNumber: data.value.mobileNumber,
      count: data.value.count,
      permittedDate: data.value.permittedDate,
      permittedTime: data.value.permittedTime
    }
    this.httpservice.addHttpPost('visitor/inviteByOwner', obj).subscribe((data: any) => {

      if (data) {
        this.presentToast("Visitor Invited Sucessfully");
        setTimeout(() => {
          window.location.reload();
        }, 3000);
      }
      else {
        this.presentToast("Unable to invite visitor");
      }




    })

    console.log(obj)
  }


  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  navigate() {
    this.router.navigate(['visitor/visitorInside']);
  }


}
