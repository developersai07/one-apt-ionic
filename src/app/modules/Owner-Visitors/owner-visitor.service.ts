import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
@Injectable({
  providedIn: 'root'
})
export class OwnerVisitorService {

  form: FormGroup = new FormGroup({
    visitorName: new FormControl('', Validators.required),
    visitorType: new FormControl('', Validators.required),
    companyName: new FormControl(''),
    mobileNumber: new FormControl(''),
    count: new FormControl('', Validators.required),
    permittedDate: new FormControl('',Validators.required),
    permittedTime: new FormControl('')
  });

  securiteInviteform: FormGroup = new FormGroup({
    visitorName: new FormControl('', Validators.required),
    visitorType: new FormControl('', Validators.required),
    companyName: new FormControl(''),
    mobileNumber: new FormControl(''),
    count: new FormControl('', Validators.required),
    childEntites: new FormControl('', Validators.required),
    grandChildEntites: new FormControl('', Validators.required),
    visitor_pic : new FormControl('') 
  });

  constructor() { }
}
