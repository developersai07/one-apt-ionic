import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {SuperTabsModule} from '@ionic-super-tabs/angular';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { Routes, RouterModule } from '@angular/router';
import {VisirorAllowComponent} from '../components/visiror-allow/visiror-allow.component';
import {VisirorInsideComponent} from '../components/visiror-inside/visiror-inside.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {VisitorRoutingModule} from '../visitor.routing.module';

@NgModule({
  declarations: [VisirorAllowComponent,VisirorInsideComponent],
  imports: [
    CommonModule,
    SuperTabsModule,
    IonicModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    VisitorRoutingModule
  ],
  entryComponents: [ ]
})
export class VisitorModule { }
