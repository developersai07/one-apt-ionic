import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HttpCallsService {
  private base_url= 'http://localhost:3000/'

  constructor(protected http: HttpClient,) { 

  }
  addHttpPost(url,data)
  {
  var redirectUrl =  this.base_url+url;
    return this.http.post(redirectUrl, data);
  }
  getHttpPost(url)
  {
  var redirectUrl =  this.base_url+url;
    return this.http.get(redirectUrl);
  }
  delete(url,data)
  {
  var redirectUrl =  this.base_url+url;
    return this.http.post(redirectUrl,data);
  }
}
