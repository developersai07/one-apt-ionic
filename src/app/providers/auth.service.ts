import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public user_role : String;
  constructor(private routes : Router){
    console.log("@@@"+this.user_role);
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(localStorage.getItem('appUserId')!= null){
     //   this.isUserLoggedIn = true;
        return true;
          }
          else
          {
            this.routes.navigate(['']);
            return false;
          }

  }

  isLoggedIn(){
    if(localStorage.getItem('appUserId')!= null){
      return true;
        }
        else
        {
          this.routes.navigate(['']);
          return false;
        }
  }

  
}
