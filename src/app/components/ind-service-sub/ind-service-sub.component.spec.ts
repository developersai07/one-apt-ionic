import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IndServiceSubComponent } from './ind-service-sub.component';

describe('IndServiceSubComponent', () => {
  let component: IndServiceSubComponent;
  let fixture: ComponentFixture<IndServiceSubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndServiceSubComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IndServiceSubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
