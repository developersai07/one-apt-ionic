import { Component, OnInit } from '@angular/core';
import { IndServiceService } from '../../services/ind-service.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
@Component({
  selector: 'app-ind-service-sub',
  templateUrl: './ind-service-sub.component.html',
  styleUrls: ['./ind-service-sub.component.scss'],
})
export class IndServiceSubComponent implements OnInit {
  public indServiceId: any;
  public serviceName : any;
  public serviceImgName: any;
  public serviceDescription: any;
  public mapServSub: any ;
  public IndticketImage: any;
public isUnitWise: any;
public imgHeight = 100;
public indServiceTypes: any =  [];
public planId;
  constructor(public indService: IndServiceService,public httpservice: HttpCallsService,  public toastController: ToastController,public router: Router,  ) {

    this.indServiceId = this.indService.getServiceId();
   
   }

  ngOnInit() {
    this.getServiceData();
    this.getIndSerPlan();
    this.getIndSerType();
    this.getMapServcePlans();
    //this.indService.initializeFormGroup();
  }
 
  getIndSerPlan(){
    this.httpservice.getHttpPost('indSerPlan/getPlansByService/'+this.indServiceId+'').subscribe((data: any) => {
console.log(data)
    })
  }
  getIndSerType(){
    this.httpservice.getHttpPost('indServiceType/getServiceTypes/'+this.indServiceId+'').subscribe((data: any) => {
      console.log(data)
      console.log("akjd jka")
      if(data.length > 0){
        console.log("sdvsdfsfakjd jka")
        this.indServiceTypes = data;
        this.indService.form.addControl('service_types_id', new FormControl('', Validators.required));
      }
    })
  }

  getServiceData(){
      this.httpservice.getHttpPost('indService/getServiceSub/'+this.indServiceId+'').subscribe((data: any) => {
        console.log(data);
       this.serviceName = data.IND_SERVICE_NAME;
       this.serviceImgName =   data.IND_IMAGE_NAME;
       this.serviceDescription =   data.DESCRIPTION;
       this.isUnitWise = data.IS_UNTI_WISE;
      // this.mapServSub =  data.mapSub;
      
         console.log("kjbjkdaj");
  
     
      });
  }


  getMapServcePlans(){
    this.httpservice.getHttpPost('indSerPlan/getPlansByService/'+this.indServiceId+'').subscribe((data: any) => {
      console.log(data);
      this.mapServSub =  data;
      console.log(this.mapServSub)
    });
  }

  checkUnitWise(event){
   var value = event.detail.value;
    if( value == '2'){
      this.imgHeight = 60;
      this.indService.form.addControl('quantity', new FormControl('', Validators.required));
      this.planId  = value;
    }
    else {
      this.indService.form.removeControl('quantity');
      this.planId  = null;
    }

  }

  onTicketFileChange(event){

      let files = event.target.files;
      console.log(files);
     // this.imgFilename = files[0].name;
      this.IndticketImage = files[0];
     
     // console.log(files);
     // this.model.files = files;
    
  }
  save(data)
  {
const formData = new FormData();
formData.append("description", data.value.ticket_description);
formData.append("ticket_date", data.value.ticket_date);
formData.append("ticket_time", data.value.ticket_time);
formData.append("service_subscription_id", data.value.service_subscription_id);
formData.append("service_types_id", data.value.service_types_id);
formData.append("quantity", data.value.quantity);
var serviceTypeId = data.value.service_types_id;
var quantity = data.value.quantity;

if (this.IndticketImage) {
  formData.append("IndticketImage", this.IndticketImage);
}
formData.append("associationId", localStorage.getItem("associationId"));
formData.append("appUserId", localStorage.getItem("appUserId"));
formData.append("created_by", localStorage.getItem("appUserId"));
formData.append("serviceId", this.indServiceId);
this.httpservice.addHttpPost('indTickets/addTicket', formData).subscribe((data: any) => {
  console.log(data);

  if(data.IND_TICKET_ID){

    this.presentToast("Ticket Raised Sucessfully");
    setTimeout(() => {
      window.location.reload();
  }, 3000);
   // this.indService.form.reset();
   // this.indService.initializeFormGroup(serviceTypeId,quantity);
   // this.presentToast("Ticket Raised Sucessfully");
   // this.router.navigate(['dashboard']);
  }
  else {
    this.presentToast("Unable to raise ticket please try again later");
  }


})


  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
