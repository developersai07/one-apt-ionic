import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KidsExitComponent } from './kids-exit.component';

describe('KidsExitComponent', () => {
  let component: KidsExitComponent;
  let fixture: ComponentFixture<KidsExitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidsExitComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KidsExitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
