import { Component, OnInit } from '@angular/core';
import { KidsExitPermitService } from 'src/app/services/kids-exit-permit.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-kids-exit',
  templateUrl: './kids-exit.component.html',
  styleUrls: ['./kids-exit.component.scss'],
})
export class KidsExitComponent implements OnInit {

  public relationsObj: any;
  segment: string;
  exitData: any;
  // tslint:disable-next-line: max-line-length
  constructor(public kidsExitService: KidsExitPermitService, public httpCallsService: HttpCallsService, public router: Router, public toastController: ToastController) { }

  ngOnInit() {
    this.getfamilyRelationType();
    this.getDetailsByUser();
    this.segment = 'Approved';
  }

  getfamilyRelationType() {
    this.httpCallsService.getHttpPost('associationFamilyRelations/getKidsAppAssociationFamilyRelations').subscribe((data: any) => {
      console.log(data);
      this.relationsObj = data;
    });
  }

  getDetailsByUser() {
    const appUserId = localStorage.getItem('appUserId');
    const associationId = localStorage.getItem('associationId');
    // tslint:disable-next-line: max-line-length
    this.httpCallsService.getHttpPost('kidsExit/getDetailsByUserAssociation/' + appUserId + '/' + associationId + '').subscribe((data: any) => {
      console.log(data);
      this.exitData = data;
    });
  }

  kidExitPermit(values) {
    const obj = {
      // kid_name: values.kid_name,
      // kid_age: values.kid_name,
      relationId: values.relation_id,
      guardian_name: values.guardian_name,
      guardian_mobile_number: values.guardian_mobile_number,
      permit_date: values.permit_date,
      permit_time: values.permit_time,
      associationId: localStorage.getItem('associationId'),
      appUserId: localStorage.getItem('appUserId'),
      created_by: localStorage.getItem('appUserId')
    };
    console.log(obj);
    this.httpCallsService.addHttpPost('kidsExit/addAppkidsExit', obj).subscribe((res: any) => {
      console.log(res);
      if (res.ID) {
        this.presentToast('Kids Exit Permit Raised Sucessfully');
        setTimeout(() => {
          window.location.reload();
        }, 3000);
      } else {
        this.presentToast('Unable to add Kids Exit Entity.');
      }
    });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
