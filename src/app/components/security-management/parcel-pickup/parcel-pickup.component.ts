import { Component, OnInit } from '@angular/core';
import { ParcelPickupService } from 'src/app/services/parcel-pickup.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-parcel-pickup',
  templateUrl: './parcel-pickup.component.html',
  styleUrls: ['./parcel-pickup.component.scss'],
})
export class ParcelPickupComponent implements OnInit {

  segment: string;
  visitorTypes: any;
  requestData: any = [];
  constructor(public parcelPickupService: ParcelPickupService, public httpCallsService: HttpCallsService, public router: Router,
    public toastController: ToastController) { }

  ngOnInit() {
    this.loadVisitorType();
    this.getPickupRequestDetails();
    this.segment = 'Approved';
  }

  loadVisitorType() {
    this.httpCallsService.getHttpPost('visitorTypes/getAll').subscribe((data: any) => {
      console.log(data);
      this.visitorTypes = data;
    });
  }

  onSubmit(value) {
    console.log(value);
    const obj = {
      visitor_type: value.visitor_type,
      company_name: value.company_name,
      guest_name: value.guest_name,
      quantity: value.quantity,
      pickup_note: value.pickup_note,
      appUserId: localStorage.getItem('appUserId'),
      associationId: localStorage.getItem('associationId'),
      created_by: localStorage.getItem('appUserId'),
    };
    console.log(obj);
    this.httpCallsService.addHttpPost('parcelPickup/raiseParcelPickupRequest', obj).subscribe((data: any) => {
      console.log(data);
      if (data.ID) {
        this.presentToast('Parcel Pickup Request Raised Sucessfully');
        setTimeout(() => {
          window.location.reload();
        }, 3000);
      } else {
        this.presentToast('Unable to Raise Parcel Pickup Request.');
      }
    });
  }

  getPickupRequestDetails() {
    const appUserId = localStorage.getItem('appUserId');
    // tslint:disable-next-line: max-line-length
    this.httpCallsService.getHttpPost('parcelPickup/getParcelPickupRequestByUserAssociation/' + appUserId + '').subscribe((data: any) => {
      console.log(data);
      this.requestData = data;
    });
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
