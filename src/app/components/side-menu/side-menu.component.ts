import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../providers/auth.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit {
  navigate: any = [];
  public loginUserName: any;
  public ImageName: any;
  public flatOwner: boolean = false;
  public flatTenant: boolean = false;
  public vendor: boolean = false;
  constructor(public authService: AuthService, public menuCtrl: MenuController, public httpservice: HttpCallsService, public router: Router, ) {

    this.authService.isUserLoggedIn.subscribe(value => {

      this.loadRoles();
      this.loadSideNav();
      // this.loadflatOwnerDetails();

      if (localStorage.getItem('flatOwnerId')) {
        this.flatOwner = true;
        this.loadflatOwnerDetails();
      }
      if (localStorage.getItem('flatTenantId')) {
        this.flatTenant = true;
        this.flatTenantById();
      }
      if (localStorage.getItem('vendorId')) {
        this.vendor = true;
        this.vendorById();
      }

      // this.userLoggedIn = value;
    });

  }

  flatTenantById() {
    this.httpservice.getHttpPost('flatTenants/getById/' + localStorage.getItem('flatTenantId') + '').subscribe((data: any) => {
      this.ImageName = data.TENANT_PIC;
      this.loginUserName = data.TENANT_FIRST_NAME
    });
  }

  vendorById() {
    this.httpservice.getHttpPost('vendor/getById/' + localStorage.getItem('vendorId') + '').subscribe((data: any) => {
      this.ImageName = data.VENDOR_PIC;

      this.loginUserName = data.VENDOR_NAME
    });
  }

  redirectProdile() {
    this.menuCtrl.toggle();
    this.router.navigate(['profile'])
  }

  loadSideNav() {
    var flatOwnerId = localStorage.getItem("flatOwnerId");
    var vendorId = localStorage.getItem("vendorId");
    console.log(flatOwnerId + "flatOwnerId")
    console.log(vendorId + 'vendorId');
    console.log("ahdhabhd")
    if (flatOwnerId) {

      console.log("flatowner")
      // this.loadflatOwnerDetails();
    }
    if (vendorId) {

      console.log("vendor")
      this.vendorDetails();
    }
  }

  vendorDetails() {
    var vendorId = localStorage.getItem("vendorId");
    this.httpservice.getHttpPost('vendor/vendorData/' + vendorId + '').subscribe((data: any) => {
      console.log("vendor info")
      console.log(data)
      console.log(data.asssociation)

      if (data.asssociation) {
        console.log("vendor info1")
        this.navigate.push({
          title: "IndServices Manage",
          url: "/indServiceManage",
          icon: "cash"
        },
          {
            title: "Log Out",
            url: "/logout",
            icon: "exit"
          })

      }
      else {
        console.log("vendor info2")
        this.navigate.push(
          {
            title: "Log Out",
            url: "/logout",
            icon: "exit"
          })
      }
    });
    // this.navigate.push({

    // })
  }

  loadflatOwnerDetails() {
    console.log("loadflatOwnerDetails");
    var flatOwnerId = localStorage.getItem("flatOwnerId");
    this.httpservice.getHttpPost('flatOwners/getById/' + flatOwnerId + '').subscribe((data: any) => {
      this.ImageName = data.OWNER_PIC;
      this.loginUserName = data.OWNER_FIRST_NAME
      console.log(data);
      if (data.IS_ASSOCIATION == true) {
        this.navigate.push({
          title: "Association",
          url: "/associationMenu",
          icon: "podium"
        },
          {
            title: "Log Out",
            url: "/logout",
            icon: "exit"
          })
      } else {
        this.navigate.push(
          {
            title: "Log Out",
            url: "/logout",
            icon: "exit"
          })

      }
    });
  }


  loadRoles() {
    this.httpservice.getHttpPost('Admin/roles/' + localStorage.getItem("appUserId") + '').subscribe((data: any) => {
      console.log(data);
      this.changeSideMenu(data);
      this.navigation(data)

    });
  }

  navigation(roles) {
    var flatowner = this.checkFlatOwner(roles);
    var flatTenant = this.checkFlatTenant(roles);
    var vendor = this.checkVendor(roles);
    var security = localStorage.getItem("securityId")
    if (flatowner) {
      //this.router.
      this.router.navigate(['dashboard']);
    }
    else if (flatTenant) {
      this.router.navigate(['dashboard']);
    }
    else if (vendor) {
      this.router.navigate(['vdashboard']);
    }
    else if(security){
      this.router.navigate(['security/tabs']);
    }
    else {
      this.router.navigate(['']);
    }
  }
  changeSideMenu(roles) {
    var flatowner = this.checkFlatOwner(roles);
    var flatTenant = this.checkFlatTenant(roles);
    var vendor = this.checkVendor(roles);

    if (flatowner) {
      this.navigate =
        [
          {
            title: "Home",
            url: "/dashboard",
            icon: "home"
          },
          {
            title: 'Profile',
            url: '/profile',
            icon: 'person-outline'
          },
          {
            title: 'Family Details',
            url: '/familyDetails',
            icon: 'person-outline'
          },
          {
            title: "Individual Area Tickets",
            url: "/assIndividualTickets",
            icon: "basket"
          },
          {
            title: "Common Area Tickets",
            url: "/assCommonTickets",
            icon: "apps"
          },
          // { 
          //   title: 'Voting',
          //   icon: '',
          //   children: [
          //     {
          //       title: 'Request Voting',
          //       url: 'voting/requstVoting',
          //       icon: ''
          //     },
          //     {
          //       title: 'Request Voting',
          //       url: 'voting/requstVoting',
          //       icon: ''
          //     }
          //   ]
          // },
          // {
          //   title: 'Request Voting',
          //   url: 'voting/requstVoting',
          //   icon: ''
          // }
        ]

    }
    else if (flatTenant) {
      this.navigate =
        [
          {
            title: "Home",
            url: "/dashboard",
            icon: "home"
          },
          {
            title: 'Profile',
            url: '/profile',
            icon: 'person-outline'
          },
          {
            title: "Individual Area Tickets",
            url: "/assIndividualTickets",
            icon: "basket"
          },
          {
            title: "Common Area Tickets",
            url: "/assCommonTickets",
            icon: "apps"
          },
          {
            title: "Log Out",
            url: "/logout",
            icon: "exit"
          },
        ]
    }

    else if (vendor) {
      this.navigate =
        [
          {
            title: "Dashboard",
            url: "/vdashboard",
            icon: "home"
          },
          {
            title: 'Profile',
            url: '/profile',
            icon: 'person-outline'
          },
          {
            title: "Individual Tickets",
            url: "/vendorIndividualTickets",
            icon: "basket"
          },
          {
            title: "Common Area Tickets",
            url: "/vendorCommonTickets",
            icon: "apps"
          },

        ]
    }

  }

  checkFlatOwner(roles) {
    var isFlatOwner = roles.find(x => x.role.ROLE_NAME == "FLAT OWNER");
    if (isFlatOwner) {
      return true
    }
    else {
      false
    }
  }

  checkFlatTenant(roles) {
    var isFlatTenant = roles.find(x => x.role.ROLE_NAME == "FLAT TENANT");
    if (isFlatTenant) {
      return true
    }
    else {
      false
    }
  }
  checkVendor(roles) {
    var isVendor = roles.find(x => x.role.ROLE_NAME == "VENDOR");
    if (isVendor) {
      return true
    }
    else {
      false
    }
  }




  ngOnInit() { }

} 