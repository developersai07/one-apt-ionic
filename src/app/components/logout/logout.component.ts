import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutComponent implements OnInit {

  constructor( public toastCtrl: ToastController) { }

  ngOnInit() {
    localStorage.clear();
    this.presentToast('Logged Out Sucessfully.');
    window.location.reload();
   
   // this.router.navigate(['login']);
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
