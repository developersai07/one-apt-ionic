import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-registration',
  templateUrl: './add-registration.component.html',
  styleUrls: ['./add-registration.component.scss'],
})
export class AddRegistrationComponent implements OnInit {

  constructor(public route : Router) { }

  ngOnInit() {}
  prevStep()
  {
    this.route.navigate(['registration'])
  }
}
