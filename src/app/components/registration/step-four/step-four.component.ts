import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { RegistrationService } from 'src/app/services/registration.service';

@Component({
  selector: 'app-step-four',
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.scss'],
})
export class StepFourComponent implements OnInit {
  vendormodel: any = {};
public vendorRegistered : any;
  constructor(public router: Router, public formBuilder: FormBuilder, public service: RegistrationService) {
    // this.service.form.addControl('vendor_registered', new FormControl('', Validators.required));
    // this.service.form.addControl('company_name', new FormControl('', Validators.required));
    // this.service.form.addControl('emp_num', new FormControl('', Validators.required));
    // this.service.form.addControl('num_clients_customers', new FormControl('', Validators.required));
   }

  ngOnInit() {}
  prevStep() {
    this.router.navigate(['step-two']);
  }

  radioCheckedVendor(data){
    //this.selectedRadioGroup = event.detail;
    this.vendorRegistered =data.detail.value;
        if(data.detail.value == 'Yes')
        {
          this.service.vendorRegistraion.addControl('vendor_registraion_number', new FormControl('', Validators.required));
          this.service.vendorRegistraion.addControl('vendor_registraion_file', new FormControl(''));
        }
        if(data.detail.value == 'No'){
          this.service.formStepthreeAssc.removeControl('vendor_registraion_number');
          this.service.formStepthreeAssc.removeControl('vendor_registraion_file');
        }
      }

      onVendorFileChange(event) {
        let files = event.target.files;
        console.log(files);
       // this.imgFilename = files[0].name;
        this.vendormodel.files = files;
        this.service.serviceVendorSetRegistratinFile(this.vendormodel.files);
       // console.log(files);
       // this.model.files = files;
      }
    
  nextStep(data: any) {
    // console.log("data: " + data.value.enrolement);
    // this.service.form.setValue({ registered: data.registered });
    // this.service.form.setValue({ company_name: data.company_name });
    // this.service.form.setValue({ emp_num: data.emp_num });
    // this.service.form.setValue({ num_clients_customers: data.num_clients_customers });
    
    this.service.vendorRegistraion.controls['vendor_registered'].setValue(data.vendor_registered);
    this.service.vendorRegistraion.controls['company_name'].setValue(data.company_name);
    this.service.vendorRegistraion.controls['emp_num'].setValue(data.emp_num);
    this.service.vendorRegistraion.controls['num_clients_customers'].setValue(data.num_clients_customers);
    console.log(this.service.form.value);
    // this.service.populatenameSection(data.value);
    this.router.navigate(['vendor-registration']);
    
  }

}
