import { Component, OnInit } from '@angular/core';
import { RegistrationService } from 'src/app/services/registration.service';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-step-three',
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.scss'],
})
export class StepThreeComponent implements OnInit {

  public appartmentRegistered: any;
  public associationRegistered: any;
  associationmodel: any = {};
  appartmentmodel: any = {};
  appartmentRgsitrationFIle : any = {};
  constructor(public router: Router, public formBuilder: FormBuilder, public service: RegistrationService) 
  {
    // this.service.form.addControl('ass_registered', new FormControl('', Validators.required));
    // this.service.form.addControl('apt_name', new FormControl('', Validators.required));
    // this.service.form.addControl('ass_name', new FormControl('', Validators.required));
    // this.service.form.addControl('no_of_flates_or_houses', new FormControl('', Validators.required));
  }

  ngOnInit() { }
  prevStep() {
    this.router.navigate(['step-two']);
  }


  openAppartmentFile() {
    document.getElementById("appartmentFile").click();
  }

  onAppartmentFileChange(event) {
    let files = event.target.files;
    console.log(files);
   // this.imgFilename = files[0].name;
    this.appartmentmodel.files = files;

    this.service.serviceAppartmentSetRegistratinFile(this.appartmentmodel.files);
   // console.log(files);
   // this.model.files = files;
  }

  onAssociationFileChange(event) {
    let files = event.target.files;
    console.log(files);
   // this.imgFilename = files[0].name;
    this.associationmodel.files = files;
    this.service.serviceAssociatinSetRegistratinFile(this.associationmodel.files);
   // console.log(files);
   // this.model.files = files;
  }

  radioCheckedAppartment(data){
//this.selectedRadioGroup = event.detail;
this.appartmentRegistered =data.detail.value;
    if(data.detail.value == 'Yes')
    {
      this.service.formStepthreeAssc.addControl('app_registraion_number', new FormControl('', Validators.required));
      this.service.formStepthreeAssc.addControl('app_registraion_file', new FormControl(''));
    }

    if (data.detail.value == 'No') {
      this.service.formStepthreeAssc.removeControl('app_registraion_number');
      this.service.formStepthreeAssc.removeControl('app_registraion_file');
    }



  }

  radioCheckedAssociation(data) {
    this.associationRegistered = data.detail.value;

    if (data.detail.value == 'Yes') {
      this.service.formStepthreeAssc.addControl('ass_registraion_number', new FormControl('', Validators.required));
      this.service.formStepthreeAssc.addControl('ass_registraion_file', new FormControl(''));
    }

    if (data.detail.value == 'No') {
      this.service.formStepthreeAssc.removeControl('ass_registraion_number');
      this.service.formStepthreeAssc.removeControl('ass_registraion_file');
    }
  }

  nextStep(data: any) {

    console.log(data);
   // this.appartmentRgsitrationFIle = this.service.getserviceAppartmentSetRegistratinFile();
    //console.log(this.appartmentRgsitrationFIle);
  //  console.log(this.appartmentRgsitrationFIle[0]);
  // console.log(this.appartmentRgsitrationFIle.FileList[0]);
    this.service.formStepthreeAssc.controls['ass_registered'].setValue(data.ass_registered);
    this.service.formStepthreeAssc.controls['app_registered'].setValue(data.app_registered);
    this.service.formStepthreeAssc.controls['apt_name'].setValue(data.apt_name);
    this.service.formStepthreeAssc.controls['ass_name'].setValue(data.ass_name);
    this.service.formStepthreeAssc.controls['no_of_flates_or_houses'].setValue(data.no_of_flates_or_houses);

    if (data.ass_registered == true) {
      this.service.formStepthreeAssc.controls['app_registraion_number'].setValue(data.app_registraion_number);
      this.service.formStepthreeAssc.controls['app_registraion_file'].setValue(data.app_registraion_file);
    }
    if (data.app_registered == true) {
      this.service.formStepthreeAssc.controls['ass_registraion_number'].setValue(data.ass_registraion_number);
      this.service.formStepthreeAssc.controls['ass_registraion_file'].setValue(data.ass_registraion_file);
    }

  
   this.router.navigate(['association-registration']);
  }



}
