import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpCallsService } from '../../../common/http-service.service';
import { RegistrationService } from 'src/app/services/registration.service';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss'],
})
export class StepTwoComponent implements OnInit {
  public countriesObj: any;
  public statesObj: any;
  public citiesObj: any;
  constructor(public router: Router, public formBuilder: FormBuilder, public service: RegistrationService, public httpservice: HttpCallsService) {
    // this.service.form.addControl('address', new FormControl('', Validators.required));
    // this.service.form.addControl('city', new FormControl('', Validators.required));
    // this.service.form.addControl('district', new FormControl('', Validators.required));
    // this.service.form.addControl('state', new FormControl('', Validators.required));
    // this.service.form.addControl('country', new FormControl('', Validators.required));
    // this.service.form.addControl('pincode', new FormControl('', Validators.required));
    // this.service.form.addControl('enrolement', new FormControl('', Validators.required));
    this.countries();
    this.states();
    this.cities();
  }

  ngOnInit() { }
  prevStep() {
    this.router.navigate(['registration']);
  }
  nextStep(data: any) {
    // console.log(data);
    this.service.formSteptwo.controls['address'].setValue(data.address);
    this.service.formSteptwo.controls['city'].setValue(data.city);
    this.service.formSteptwo.controls['district'].setValue(data.district);
    this.service.formSteptwo.controls['state'].setValue(data.state);
    this.service.formSteptwo.controls['country'].setValue(data.country);
    this.service.formSteptwo.controls['pincode'].setValue(data.pincode);
    this.service.formSteptwo.controls['enrolement'].setValue(data.enrolement);
    this.service.formSteptwo.controls['landMark'].setValue(data.landMark);

    console.log(this.service.formSteptwo.value);
    if (data.enrolement == 'association') {
      this.router.navigate(['step-three']);
    } else if (data.enrolement == 'vendor') {
      this.vendorcontrol();
      this.router.navigate(['step-four']);
    }
  }
  vendorcontrol() {
    this.service.form.removeControl('ass_registered');
    this.service.form.removeControl('apt_name');
    this.service.form.removeControl('ass_name');
    this.service.form.removeControl('no_of_flates_or_houses');
  }
  countries() {
    this.httpservice.getHttpPost('countries/getAll').subscribe((data: any) => {
      console.log(data);
      this.countriesObj = data;
    });
  }
  states() {
    this.httpservice.getHttpPost('states/getAll').subscribe((data: any) => {
      console.log(data);
      this.statesObj = data;
    });
  }
  cities() {
    this.httpservice.getHttpPost('cities/getAll').subscribe((data: any) => {
      console.log(data);
      this.citiesObj = data;
    });
  }
  onChangecountry(countryIs) {
    console.log("ojwer");
    console.log(countryIs);
    this.httpservice.getHttpPost('states/getStates/' + countryIs + '').subscribe((data: any) => {
      console.log(data);
      this.statesObj = data;
    });
  }
  onChangestate(stateIs) {
    console.log("ojwer");
    console.log(stateIs);
    this.httpservice.getHttpPost('cities/getCities/' + stateIs + '').subscribe((data: any) => {
      console.log(data);
      this.citiesObj = data;
    });
  }
 
}
