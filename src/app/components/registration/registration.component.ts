import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators,FormControl } from "@angular/forms";
import { RegistrationService } from '../../services/registration.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {

  constructor(public formBuilder: FormBuilder, public service: RegistrationService, public router: Router) {

  }

  ngOnInit() { }
  nextStep(data: any) {
    this.service.populatenameSection(data.value);
    this.router.navigate(['step-two']);

  }


  goToLogin(){
    this.router.navigate(['']);
  }
}
