import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from '../../../common/http-service.service';
import { RegistrationService } from '../../../services/registration.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss'],
})
export class PlansComponent implements OnInit {
  public plansObj: any;
  public appartmentRgsitrationFIle: any = {};
  public associationRgsitrationFIle: any = {};
  public vendorRegsitrationFIle: any = {};
  public perLoginAmount: any;
  public netAmount: any;
  constructor(public router: Router, public httpservice: HttpCallsService, public service: RegistrationService,
              public toastController: ToastController) { }

  ngOnInit() {
    this.httpservice.getHttpPost('plans/getAll').subscribe((data: any) => {
      console.log(data);

      this.plansObj = data;
    });

    this.getPerLoginAmount();
  }


  getPerLoginAmount(){
    this.httpservice.getHttpPost('adminSetups/getPerLoginAmount').subscribe((data: any) => {
     this.perLoginAmount = data.AMOUNT;
     this.netAmount =   this.perLoginAmount;
    });

  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
  submit(data) {
    console.log(data);
    this.service.plans.controls['plan'].setValue(data.plan);
    var value = this.service.formSteptwo.value['enrolement'];
    if (value == 'association') {
      this.addAssociation();
    }

    if (value == 'vendor') {
      this.addVendorAssociation();
    }


  }

  addVendorAssociation() {

    this.vendorRegsitrationFIle = this.service.getserviceVendorSetRegistratinFile();
    let data = {
      contact_person_name: this.service.form.value['name'],
      email: this.service.form.value['email'],
      phone_number: this.service.form.value['mobile'],
      //2
      address: this.service.formSteptwo.value['address'],
      cities_id: this.service.formSteptwo.value['city'],
      distrcit: this.service.formSteptwo.value['district'],
      states_id: this.service.formSteptwo.value['state'],
      country_id: this.service.formSteptwo.value['country'],
      pin_code: this.service.formSteptwo.value['pincode'],
      landmark: this.service.formSteptwo.value['landMark'],

      //4

      vendor_registered: this.service.vendorRegistraion.value['vendor_registered'],
      company_name: this.service.vendorRegistraion.value['company_name'],
      emp_num: this.service.vendorRegistraion.value['emp_num'],
      num_clients_customers: this.service.vendorRegistraion.value['num_clients_customers'],
      vendor_registraion_number: this.service.vendorRegistraion.value['vendor_registraion_number'],
      vendor_registraion_file: this.service.vendorRegistraion.value['vendor_registraion_file'],

      //vendor registraion 

      vendor_name: this.service.vendorInfo.value['vendor_name'],
      vendor_email: this.service.vendorInfo.value['vendor_email'],
      vendor_mobile: this.service.vendorInfo.value['vendor_mobile'],
      vendor_address: this.service.vendorInfo.value['vendor_address'],
      // app sub registraion
      data_range: this.service.vendorSubRegistraion.value['data_range'],
      no_of_report_cards: this.service.vendorSubRegistraion.value['no_of_report_cards'],
      no_of_clients_customers_for_registration: this.service.vendorSubRegistraion.value['no_of_clients_customers_for_registration'],
      no_of_employee_login: this.service.vendorSubRegistraion.value['no_of_employee_login'],
      email_mode: this.service.vendorSubRegistraion.value['email_mode'],
      sms_mode: this.service.vendorSubRegistraion.value['sms_mode'],
      whatsApp_mode: this.service.vendorSubRegistraion.value['whatsApp_mode'],
      act_req_communication: this.service.vendorSubRegistraion.value['act_req_communication'],
      plan: this.service.plans.value['plan']
    }


    const formData = new FormData();

    formData.append("contact_person_name", this.service.form.value['name']);
    formData.append("email", this.service.form.value['email']);
    formData.append("phone_number", this.service.form.value['phone_number']);



    formData.append("address", this.service.formSteptwo.value['address']);
    formData.append("cities_id", this.service.formSteptwo.value['city']);
    formData.append("distrcit", this.service.formSteptwo.value['district']);
    formData.append("states_id", this.service.formSteptwo.value['state']);
    formData.append("country_id", this.service.formSteptwo.value['country']);
    formData.append("pin_code", this.service.formSteptwo.value['pincode']);
    formData.append("landmark", this.service.formSteptwo.value['landMark']);


    formData.append("vendor_registered", this.service.vendorRegistraion.value['vendor_registered']);
    formData.append("company_name", this.service.vendorRegistraion.value['company_name']);
    formData.append("emp_num", this.service.vendorRegistraion.value['emp_num']);
    formData.append("num_clients_customers", this.service.vendorRegistraion.value['num_clients_customers']);
    formData.append("vendor_registraion_number", this.service.vendorRegistraion.value['vendor_registraion_number']);
    if (this.vendorRegsitrationFIle) {
      formData.append("vendorRegistrationFile", this.vendorRegsitrationFIle[0]);
    }
    formData.append("vendor_name", this.service.vendorInfo.value['vendor_name']);
    formData.append("vendor_email", this.service.vendorInfo.value['vendor_email']);
    formData.append("vendor_mobile", this.service.vendorInfo.value['vendor_mobile']);
    formData.append("vendor_address", this.service.vendorInfo.value['vendor_address']);

    //

    formData.append("data_range", this.service.vendorSubRegistraion.value['data_range']);
    formData.append("no_of_report_cards", this.service.vendorSubRegistraion.value['no_of_report_cards']);
    formData.append("no_of_clients_customers_for_registration", this.service.vendorSubRegistraion.value['no_of_clients_customers_for_registration']);
    formData.append("no_of_employee_login", this.service.vendorSubRegistraion.value['no_of_employee_login']);
    formData.append("email_mode", this.service.vendorSubRegistraion.value['email_mode']);
    formData.append("sms_mode", this.service.vendorSubRegistraion.value['sms_mode']);
    formData.append("whatsApp_mode", this.service.vendorSubRegistraion.value['whatsApp_mode']);
    formData.append("act_req_communication", this.service.vendorSubRegistraion.value['act_req_communication']);

    formData.append("plan", this.service.plans.value['plan']);
    //plan : this.service.plans.value['plan']

    console.log(formData);

    console.log(data);

    this.httpservice.addHttpPost('vendor/vendorEnrollemnt', data).subscribe((data: any) => {
      console.log(data);

      if (data) {
        this.presentToast("Login Credentials sent to your mail");
        setTimeout(() => {
          window.location.reload();
      }, 3000);
     
      //  this.router.navigate(['']);
      }
    })


  }

  addAssociation() {
    let data = {
      // contact_person_name :this.service.form.value['name'],
      // email :this.service.form.value['email'],
      // phone_number :this.service.form.value['mobile'],
      // //2
      // address : this.service.formSteptwo.value['address'],
      // cities_id : this.service.formSteptwo.value['city'],
      // distrcit : this.service.formSteptwo.value['district'],
      // states_id : this.service.formSteptwo.value['state'],
      // country_id : this.service.formSteptwo.value['country'],
      // pin_code : this.service.formSteptwo.value['pincode'],
      // landmark: this.service.formSteptwo.value['landMark'],
      // //3  
      // is_registration_number : this.service.formStepthreeAssc.value['ass_registered'],
      // is_registered : this.service.formStepthreeAssc.value['app_registered'],
      // appartment_name : this.service.formStepthreeAssc.value['apt_name'],
      // association_name : this.service.formStepthreeAssc.value['ass_name'],
      // app_registration_number : this.service.formStepthreeAssc.value['app_registraion_number'],
      // ass_registraion_number : this.service.formStepthreeAssc.value['ass_registraion_number'],
      // app_registraion_file : this.service.formStepthreeAssc.value['app_registraion_file'],
      // ass_registraion_file : this.service.formStepthreeAssc.value['ass_registraion_file'],
      //ass registrationo on 


      //  presiden_name : this.service.associationPresident.value['ass_president_name'],
      //  president_email : this.service.associationPresident.value['ass_president_email'],
      //  president_phone_number : this.service.associationPresident.value['ass_president_mobile'],
      //  prseident_address : this.service.associationPresident.value['ass_president_address'],
      //  number_of_parking_slots : this.service.associationPresident.value['no_parking_slots'],
      //  two_wheeler_parking_slots : this.service.associationPresident.value['no_of_2_wheelers'],
      //  four_wheeler_parking_slots : this.service.associationPresident.value['no_of_4_wheelers'],
      //  ambulance_parking : this.service.associationPresident.value['ambulance_parking'],
      //  visitor_vehicle : this.service.associationPresident.value['visitor_vehicle'],


      // app sub registraion
      data_range: this.service.associationSubRegistraion.value['data_range'],
      no_of_report_cards: this.service.associationSubRegistraion.value['no_of_report_cards'],
      number_flats_houses: this.service.associationSubRegistraion.value['no_of_flats_for_registration'],
      no_of_employee_login: this.service.associationSubRegistraion.value['no_of_employee_login'],
      email_mode: this.service.associationSubRegistraion.value['email_mode'],
      sms_mode: this.service.associationSubRegistraion.value['sms_mode'],
      whatsApp_mode: this.service.associationSubRegistraion.value['whatsApp_mode'],
      act_req_communication: this.service.associationSubRegistraion.value['act_req_communication'],
      ass_payment: this.service.associationSubRegistraion.value['ass_payment'],


      // plans

      plan: this.service.plans.value['plan']


    }




    this.appartmentRgsitrationFIle = this.service.getserviceAppartmentSetRegistratinFile();

    this.associationRgsitrationFIle = this.service.getserviceAssociationSetRegistratinFile();

    const formData = new FormData();

    formData.append("contact_person_name", this.service.form.value['name']);
    formData.append("email", this.service.form.value['email']);
    formData.append("phone_number", this.service.form.value['mobile']);



    formData.append("address", this.service.formSteptwo.value['address']);
    formData.append("cities_id", this.service.formSteptwo.value['city']);
    formData.append("distrcit", this.service.formSteptwo.value['district']);
    formData.append("states_id", this.service.formSteptwo.value['state']);
    formData.append("country_id", this.service.formSteptwo.value['country']);
    formData.append("pin_code", this.service.formSteptwo.value['pincode']);
    formData.append("landmark", this.service.formSteptwo.value['landMark']);

    //3

    formData.append("is_registration_number", this.service.formStepthreeAssc.value['ass_registered']);
    formData.append("is_registered", this.service.formStepthreeAssc.value['app_registered']);
    formData.append("appartment_name", this.service.formStepthreeAssc.value['apt_name']);
    formData.append("association_name", this.service.formStepthreeAssc.value['ass_name']);
    formData.append("app_registration_number", this.service.formStepthreeAssc.value['app_registraion_number']);
    formData.append("ass_registraion_number", this.service.formStepthreeAssc.value['ass_registraion_number']);

    console.log(this.appartmentRgsitrationFIle);
    if (this.appartmentRgsitrationFIle) {
      console.log("appartmentRegistrationFile");
      formData.append("appartmentRegistrationFile", this.appartmentRgsitrationFIle[0]);
    }
    if (this.associationRgsitrationFIle) {
      formData.append("associationRegistrationFile", this.associationRgsitrationFIle[0]);
    }




    //4


    formData.append("presiden_name", this.service.associationPresident.value['ass_president_name']);
    formData.append("president_email", this.service.associationPresident.value['ass_president_email']);
    formData.append("president_phone_number", this.service.associationPresident.value['ass_president_mobile']);
    formData.append("prseident_address", this.service.associationPresident.value['ass_president_address']);
    formData.append("number_of_parking_slots", this.service.associationPresident.value['no_parking_slots']);
    formData.append("two_wheeler_parking_slots", this.service.associationPresident.value['no_of_2_wheelers']);
    formData.append("four_wheeler_parking_slots", this.service.associationPresident.value['no_of_4_wheelers']);
    formData.append("ambulance_parking", this.service.associationPresident.value['ambulance_parking']);
    formData.append("visitor_vehicle", this.service.associationPresident.value['visitor_vehicle']);

    // 5


    formData.append("data_range", this.service.associationSubRegistraion.value['data_range']);
    formData.append("no_of_report_cards", this.service.associationSubRegistraion.value['no_of_report_cards']);
    formData.append("number_flats_houses", this.service.associationSubRegistraion.value['no_of_flats_for_registration']);
    formData.append("no_of_employee_login", this.service.associationSubRegistraion.value['no_of_employee_login']);
    formData.append("email_mode", this.service.associationSubRegistraion.value['email_mode']);
    formData.append("sms_mode", this.service.associationSubRegistraion.value['sms_mode']);
    formData.append("whatsApp_mode", this.service.associationSubRegistraion.value['whatsApp_mode']);
    formData.append("act_req_communication", this.service.associationSubRegistraion.value['act_req_communication']);
    formData.append("ass_payment", this.service.associationSubRegistraion.value['ass_payment']);


    formData.append("plan", this.service.plans.value['plan']);
    console.log(formData);
    this.httpservice.addHttpPost('association/associationEnrollemnt', formData).subscribe((data: any) => {
      console.log(data);

      if (data) {
        this.presentToast('Login Credentials sent to your mail');
        //window.location.reload();
       // this.router.navigate(['']);

       setTimeout(() => {
        window.location.reload();
    }, 3000);
      }
    })



  }
  prevStep() {
    const value = this.service.formSteptwo.value['enrolement'];
    console.log(value);
    // var value = this.service.formSteptwo.controls['enrolement'].value();
    if (value === 'association') {
      this.router.navigate(['association-sub-registration']);
    }
    if (value === 'vendor') {
      this.router.navigate(['vendor-registration']);
    }

  }
}
