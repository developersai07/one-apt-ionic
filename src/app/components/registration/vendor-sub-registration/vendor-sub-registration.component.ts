import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { RegistrationService } from 'src/app/services/registration.service';

@Component({
  selector: 'app-vendor-sub-registration',
  templateUrl: './vendor-sub-registration.component.html',
  styleUrls: ['./vendor-sub-registration.component.scss'],
})
export class VendorSubRegistrationComponent implements OnInit {

  constructor(public router: Router, public formBuilder: FormBuilder, public service: RegistrationService) {
    // this.service.form.addControl('data_range', new FormControl('', Validators.required));
    // this.service.form.addControl('no_of_report_cards', new FormControl('', Validators.required));
    // this.service.form.addControl('no_of_clients_customers_for_registration', new FormControl('', Validators.required));
    // this.service.form.addControl('no_of_employee_login', new FormControl('', Validators.required));
    // this.service.form.addControl('email_mode', new FormControl('', Validators.required));
    // this.service.form.addControl('sms_mode', new FormControl('', Validators.required));
    // this.service.form.addControl('whatsApp_mode', new FormControl('', Validators.required));
    // this.service.form.addControl('act_req_communication', new FormControl('', Validators.required));
   }

  ngOnInit() { }
  prevStep() {
    this.router.navigate(['vendor-registration']);
  }
  nextStep(data: any) {
    this.service.vendorSubRegistraion.controls['data_range'].setValue(data.data_range);
    this.service.vendorSubRegistraion.controls['no_of_report_cards'].setValue(data.no_of_report_cards);
    this.service.vendorSubRegistraion.controls['no_of_clients_customers_for_registration'].setValue(data.no_of_clients_customers_for_registration);
    this.service.vendorSubRegistraion.controls['no_of_employee_login'].setValue(data.no_of_employee_login);
    this.service.vendorSubRegistraion.controls['email_mode'].setValue(data.email_mode);
    this.service.vendorSubRegistraion.controls['sms_mode'].setValue(data.sms_mode);
    this.service.vendorSubRegistraion.controls['whatsApp_mode'].setValue(data.whatsApp_mode);
    this.service.vendorSubRegistraion.controls['act_req_communication'].setValue(data.act_req_communication);
    this.router.navigate(['/plans'])
  }
}
