import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { RegistrationService } from 'src/app/services/registration.service';

@Component({
  selector: 'app-association-registration',
  templateUrl: './association-registration.component.html',
  styleUrls: ['./association-registration.component.scss'],
})
export class AssociationRegistrationComponent implements OnInit {

  constructor(public router: Router, public formBuilder: FormBuilder, public service: RegistrationService) {
    // this.service.form.addControl('reg_certificate', new FormControl('', Validators.required));
    // this.service.form.addControl('ass_president_name', new FormControl('', Validators.required));
    // this.service.form.addControl('ass_president_email', new FormControl('', Validators.required));
    // this.service.form.addControl('ass_president_mobile', new FormControl('', Validators.required));
    // this.service.form.addControl('ass_president_address', new FormControl('', Validators.required));
    // this.service.form.addControl('no_of_2_wheelers', new FormControl('', Validators.required));
    // this.service.form.addControl('no_of_4_wheelers', new FormControl('', Validators.required));
    // this.service.form.addControl('ambulance_parking', new FormControl('', Validators.required));
    // this.service.form.addControl('common_area_service', new FormControl('', Validators.required));
    // this.service.form.addControl('visitor_vehicle', new FormControl('', Validators.required));
   }

  ngOnInit() {}
  prevStep() {
    this.router.navigate(['step-three']);
  }
  nextStep(data: any) {

    console.log(data);
    // console.log(data.value);
    // this.service.form.setValue({ reg_certificate: data.reg_certificate });
    // this.service.form.setValue({ ass_president_name: data.ass_president_name });
    // this.service.form.setValue({ ass_president_email: data.ass_president_email });
    // this.service.form.setValue({ ass_president_mobile: data.ass_president_mobile });
    // this.service.form.setValue({ ass_president_address: data.ass_president_address });
    // this.service.form.setValue({ no_of_2_wheelers: data.no_of_2_wheelers });
    // this.service.form.setValue({ no_of_4_wheelers: data.no_of_4_wheelers });
    // this.service.form.setValue({ ambulance_parking: data.ambulance_parking });
    // this.service.form.setValue({ common_area_service: data.common_area_service });
    // this.service.form.setValue({ visitor_vehicle: data.visitor_vehicle });
    this.service.associationPresident.controls['ass_president_name'].setValue(data.ass_president_name);
    this.service.associationPresident.controls['ass_president_email'].setValue(data.ass_president_email);
    this.service.associationPresident.controls['ass_president_mobile'].setValue(data.ass_president_mobile);
    this.service.associationPresident.controls['ass_president_address'].setValue(data.ass_president_address);
    this.service.associationPresident.controls['no_parking_slots'].setValue(data.no_parking_slots);
    this.service.associationPresident.controls['no_of_2_wheelers'].setValue(data.no_of_2_wheelers);
    this.service.associationPresident.controls['no_of_4_wheelers'].setValue(data.no_of_4_wheelers);
    this.service.associationPresident.controls['ambulance_parking'].setValue(data.ambulance_parking);
    //this.service.associationPresident.controls['common_area_service'].setValue(data.common_area_service);
    this.service.associationPresident.controls['visitor_vehicle'].setValue(data.visitor_vehicle);
   // console.log(this.service.form.value);
    // this.service.populatenameSection(data.value);
    this.router.navigate(['association-sub-registration']);
  }

}
