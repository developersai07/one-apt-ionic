import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { RegistrationService } from 'src/app/services/registration.service';

@Component({
  selector: 'app-vendor-registration',
  templateUrl: './vendor-registration.component.html',
  styleUrls: ['./vendor-registration.component.scss'],
})
export class VendorRegistrationComponent implements OnInit {

  constructor(public router: Router, public formBuilder: FormBuilder, public service: RegistrationService) { 
    // this.service.form.addControl('reg_certificate', new FormControl('', Validators.required));
    // this.service.form.addControl('vendor_name', new FormControl('', Validators.required));
    // this.service.form.addControl('vendor_email', new FormControl('', Validators.required));
    // this.service.form.addControl('vendor_mobile', new FormControl('', Validators.required));
    // this.service.form.addControl('vendor_address', new FormControl('', Validators.required));
    // this.service.form.addControl('services_provided', new FormControl('', Validators.required));
  }

  ngOnInit() {}
  prevStep() {
    this.router.navigate(['step-four']);
  }

  nextStep(data: any) {
    // console.log(data.value);
    
    // this.service.form.setValue({ reg_certificate: data.reg_certificate });
    // this.service.form.setValue({ vendor_name: data.vendor_name });
    // this.service.form.setValue({ vendor_email: data.vendor_email });
    // this.service.form.setValue({ vendor_mobile: data.vendor_mobile });
    // this.service.form.setValue({ vendor_address: data.vendor_address });
    // this.service.form.setValue({ services_provided: data.services_provided });

    this.service.vendorInfo.controls['vendor_name'].setValue(data.vendor_name);
    this.service.vendorInfo.controls['vendor_email'].setValue(data.vendor_email);
    this.service.vendorInfo.controls['vendor_mobile'].setValue(data.vendor_mobile);
    this.service.vendorInfo.controls['vendor_address'].setValue(data.vendor_address);
  //  this.service.vendorInfo.controls['services_provided'].setValue(data.services_provided);
    console.log(this.service.vendorInfo.value);
    // this.service.populatenameSection(data.value);
    this.router.navigate(['vendor-sub-registration']);
  }

}
