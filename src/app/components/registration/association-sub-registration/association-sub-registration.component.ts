import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { RegistrationService } from 'src/app/services/registration.service';

@Component({
  selector: 'app-association-sub-registration',
  templateUrl: './association-sub-registration.component.html',
  styleUrls: ['./association-sub-registration.component.scss'],
})
export class AssociationSubRegistrationComponent implements OnInit {

  constructor(public router: Router, public formBuilder: FormBuilder, public service: RegistrationService) { 
    // this.service.form.addControl('data_range', new FormControl('', Validators.required));
    // this.service.form.addControl('no_of_report_cards', new FormControl('', Validators.required));
    // this.service.form.addControl('no_of_flats_for_registration', new FormControl('', Validators.required));
    // this.service.form.addControl('no_of_employee_login', new FormControl('', Validators.required));
    // this.service.form.addControl('email_mode', new FormControl('', Validators.required));
    // this.service.form.addControl('sms_mode', new FormControl('', Validators.required));
    // this.service.form.addControl('whatsApp_mode', new FormControl('', Validators.required));
    // this.service.form.addControl('act_req_communication', new FormControl('', Validators.required));
    // this.service.form.addControl('ass_payment', new FormControl('', Validators.required));
  }

  ngOnInit() { }
  prevStep() {
    this.router.navigate(['association-registration']);
  }
  nextStep(data: any) {
    // console.log(data.value);
    // this.service.form.setValue({ data_range: data.data_range });
    // this.service.form.setValue({ no_of_report_cards: data.no_of_report_cards });
    // this.service.form.setValue({ no_of_flats_for_registration: data.no_of_flats_for_registration });
    // this.service.form.setValue({ no_of_employee_login: data.no_of_employee_login });
    // this.service.form.setValue({ email_mode: data.email_mode });
    // this.service.form.setValue({ sms_mode: data.sms_mode });
    // this.service.form.setValue({ whatsApp_mode: data.whatsApp_mode });
    // this.service.form.setValue({ act_req_communication: data.act_req_communication });
    // this.service.form.setValue({ ass_payment: data.ass_payment });

    this.service.associationSubRegistraion.controls[ 'data_range' ].setValue(data.data_range);
    this.service.associationSubRegistraion.controls[ 'no_of_report_cards' ].setValue(data.no_of_report_cards);
    this.service.associationSubRegistraion.controls['no_of_flats_for_registration'].setValue(data.no_of_flats_for_registration);
    this.service.associationSubRegistraion.controls['no_of_employee_login'].setValue(data.no_of_employee_login);
    this.service.associationSubRegistraion.controls['email_mode'].setValue(data.email_mode);
    this.service.associationSubRegistraion.controls['sms_mode'].setValue(data.sms_mode);
    this.service.associationSubRegistraion.controls['whatsApp_mode'].setValue(data.whatsApp_mode);
    this.service.associationSubRegistraion.controls['act_req_communication'].setValue(data.act_req_communication);
    this.service.associationSubRegistraion.controls['ass_payment'].setValue(data.ass_payment);
    console.log(this.service.associationSubRegistraion.value);

    this.router.navigate(['/plans'])
    
  }
}
