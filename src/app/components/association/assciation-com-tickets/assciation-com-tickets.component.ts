import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-assciation-com-tickets',
  templateUrl: './assciation-com-tickets.component.html',
  styleUrls: ['./assciation-com-tickets.component.scss'],
})
export class AssciationComTicketsComponent implements OnInit {
  public comTickets: any;

  constructor(public httpservice: HttpCallsService, public toastCtrl: ToastController, public router: Router, ) {
    this.assComTickets();
    // this.assComTIcketsCompleted()
  }

  assComTickets() {
    this.httpservice.getHttpPost('commonAreaTickets/getComTicketByAsscoiation/' + localStorage.getItem('associationId') + '').subscribe((data: any) => {
      //  this.indTickets = data;
      //       this.changeSideMenu(data);
      //  this.navigation(data)

      console.log(data)
      this.comTickets = data

    });
  }


  ngOnInit() { }


  workCompltedVerify(id) {

    console.log(id)

    console.log("common area ticket Id " + id)


    this.httpservice.getHttpPost('commonAreaTickets/workCompleteVerify/' + id + '/' + localStorage.getItem('appUserId') + '').subscribe((data: any) => {
      //  this.indTickets = data;
      //       this.changeSideMenu(data);
      //  this.navigation(data)

      // console.log(data)
      // this.comTickets = data
      if (data) {
        this.presentToast('Ticked Closed Successfully');
        this.router.navigate(['dashboard']);
      }

    });

  }

  accept(ticketId) {
    this.httpservice.getHttpPost('commonAreaTickets/mapAssociationComTicketAction/' + ticketId + '/' + localStorage.getItem('appUserId') + '/1/' + localStorage.getItem("associationId") + '').subscribe((data: any) => {
      if (data) {
        this.presentToast('Ticket Accepted Succesfully ');
        this.router.navigate(['dashboard']);
      }
    });
  }

  getComAssociationAction(data) {
    var mapComTicketArray = [];
    mapComTicketArray = data;
    var mapCom = mapComTicketArray.find(x => x.appUser.APP_USER_ID == localStorage.getItem("appUserId"));

    if (mapCom) {
      return true;
    }
    else {
      return false;
    }
  }

  getComAssociationStatus(data) {
    var mapComTicketArray = [];
    mapComTicketArray = data;
    console.log(mapComTicketArray); 
    var mapCom = mapComTicketArray.find(x => x.appUser.APP_USER_ID == localStorage.getItem("appUserId"));
    console.log(mapCom);

    if (mapCom) {

      if (mapCom.IS_ACCEPTED == true) {
        return 'Accepted';
      }
      else {
        return 'Rejected';
      }

    }
    else {
      return false;
    }
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
  reject(ticketId) {
    this.httpservice.getHttpPost('commonAreaTickets/mapAssociationComTicketAction/' + ticketId + '/' + localStorage.getItem('appUserId') + '/2').subscribe((data: any) => {
      if (data) {
        this.presentToast('Ticket Rejected Succesfully ');
        this.router.navigate(['dashboard']);
      }
    });
  }

}
