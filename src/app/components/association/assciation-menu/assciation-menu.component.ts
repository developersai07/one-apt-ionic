import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-assciation-menu',
  templateUrl: './assciation-menu.component.html',
  styleUrls: ['./assciation-menu.component.scss'],
})
export class AssciationMenuComponent implements OnInit {

  constructor(public router: Router, ) { }

  ngOnInit() { }

  viewAssComService() {
    this.router.navigate(['associationComTickets']);
  }

  kidsExitService() {
    this.router.navigate(['kidsExitPermit']);
  }

  parcelPickupService() {
    this.router.navigate(['parcelPickup']);
  }

  Visitors() {
    this.router.navigate(['visitor/visitorRequest']);
  }

  votingService() {
    this.router.navigate(['voting/requstVoting']);
  }

}
