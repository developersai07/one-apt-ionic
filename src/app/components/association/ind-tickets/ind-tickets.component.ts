import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { IndTicketAdditionalAmountService } from '../../../services/ind-ticket-additional-amount.service';
@Component({
  selector: 'app-ind-tickets',
  templateUrl: './ind-tickets.component.html',
  styleUrls: ['./ind-tickets.component.scss'],
})
export class IndTicketsComponent implements OnInit {
  public indTickets: any;

  // tslint:disable-next-line: max-line-length
  constructor(public httpservice: HttpCallsService, public toastCtrl: ToastController, public router: Router, public indTicketAdiitionalAmountService: IndTicketAdditionalAmountService) {
  }

  ngOnInit() {
    this.individualTickets();
  }

  ionViewWillEnter() {

    console.log("Dash1")
    this.ngOnInit()
  }


  individualTickets() {
    this.httpservice.getHttpPost('indTickets/appUserIndividualTickets/' + localStorage.getItem('appUserId') + '').subscribe((data: any) => {
      this.indTickets = data;
      //       this.changeSideMenu(data);
      //  this.navigation(data)

    });
  }



  workCompltedVerify(data, ticketId, plan) {

    var mapVendorId = data.find(x => x.IS_ACTIVE == true);
    mapVendorId.id;
    this.workVerified(mapVendorId.id, ticketId, plan);

  }

  checkDate(date) {
    date.vendorWorkCompleted
  }
  getVendorWorkCmopleted(data) {
    var ticketOtpArray = [];
    ticketOtpArray = data;
    var ticketOtp;
    if (ticketOtpArray.length > 0) {
      for (var i = 0; i < ticketOtpArray.length; i++) {
        ticketOtp = ticketOtpArray[i];
        var checkres = this.comapreDate(ticketOtp.OTP_DATE);
        if (checkres == true) {
          if (ticketOtp.IS_WORK_COMPLETED == true && ticketOtp.IS_USER_VERIFIED == false) {

            return ticketOtp;
          }
        }
      }
    }
    else {
      return null
    }

  }


  getTicketOtp(data) {
    var ticketOtpArray = [];
    ticketOtpArray = data;
    var ticketOtp;
    if (ticketOtpArray.length > 0) {
      for (var i = 0; i < ticketOtpArray.length; i++) {
        ticketOtp = ticketOtpArray[i];
        var checkres = this.comapreDate(ticketOtp.OTP_DATE);
        if (checkres == true) {
          if (ticketOtp.IS_ACTIVE == true && ticketOtp.IS_USER_VERIFIED == false) {

            return ticketOtp.OTP_NUMBER;
          }
        }
      }
    }
    else {
      return null
    }

  }

  resendOtp(ticketId) {
    var cr_date = new Date();
    var cur_date = cr_date.getDate();
    var cur_month = cr_date.getMonth();
    var cur_year = cr_date.getFullYear();
    cur_month = cur_month + 1;
    var formattedDate = cur_year + '-' + cur_month + '-' + cur_date;
    let data = {
      ticketId: ticketId,
      otpDate: formattedDate
    }
    this.httpservice.addHttpPost('indTickets/resendOtp', data).subscribe((data: any) => {
      console.log(data);

      if (data) {
        if (data.msg == "Work Already Completed") {
          this.presentToast('Work Completed  for today. please come tomorrow');
        }
        else {

          console.log("okjdk")
          this.presentToast('Otp Genrated  ');
          this.router.navigate(['dashboard']);

        }

      }

      // this.vendorIndTickets = data;
      //       this.changeSideMenu(data);
      //  this.navigation(data)

    });
  }

  getTicketAssigmed(data){

    var mapIndVendor = [];
    mapIndVendor = data;
    var ticketOtp;
    if (mapIndVendor.length > 0) {
      for (var i = 0; i < mapIndVendor.length; i++) {
      var  checkres = mapIndVendor[i].IS_ACTIVE;
       // var checkres = this.comapreDate(ticketOtp.OTP_DATE);
        if (checkres == true) {
          return true;
          // if (ticketOtp.IS_ACTIVE == true && ticketOtp.IS_USER_VERIFIED == false) {

          //   return ticketOtp.OTP_NUMBER;
          // }
        }
      }
    }
    else {
      return null
    }

  }

  indTicketAmount(data) {


    var addionalAmount = data.indAddtionAmount;

    var ticketAmount = data.AMOUNT;


    if (addionalAmount.length > 0) {

      for (var i = 0; i < addionalAmount.length; i++) {
        addionalAmount[i].AMOUNT;
        ticketAmount = ticketAmount + addionalAmount[i].AMOUNT;
      }

      console.log(ticketAmount);
      return ticketAmount;
    }
    else {
      return ticketAmount;
    }


  }

  workVerified(id, ticketId, plan) {
    var cr_date = new Date();
    var cur_date = cr_date.getDate();
    var cur_month = cr_date.getMonth();
    var cur_year = cr_date.getFullYear();
    cur_month = cur_month + 1;
    var formattedDate = cur_year + '-' + cur_month + '-' + cur_date;
    let data = {
      ticketId: ticketId,
      otpDate: formattedDate,
      plan: plan
    }

    this.httpservice.addHttpPost('indTickets/ownerWorkCompleted', data).subscribe((data: any) => {
      console.log(data);
      this.presentToast('Work Completed and updated to Vendor ');
      this.router.navigate(['dashboard']);
      // this.vendorIndTickets = data;
      //       this.changeSideMenu(data);
      //  this.navigation(data)

    });
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
  comapreDate(date) {

    var otpDate = new Date(date);
    var cr_date = new Date();
    var otp_date = otpDate.getDate();
    var otp_month = otpDate.getMonth();
    var otp_year = otpDate.getFullYear();
    var cur_date = cr_date.getDate();
    var cur_month = cr_date.getMonth();
    var cur_year = cr_date.getFullYear();
    cur_month = cur_month + 1;
    otp_month = otp_month + 1;

    console.log(cur_month);

    var formattedDate = cur_year + '-' + cur_month + '-' + cur_date;
    var oDate = otp_year + '-' + otp_month + '-' + otp_date;

    console.log("formattedDate" + formattedDate);
    console.log("oDate" + oDate);
    console.log(oDate);
    if (formattedDate == oDate) {
      return true
    }
    else {
      return null;
    }

  }

  checkIsActive(data) {
    var isActive = data.find(x => x.IS_ACTIVE == true);
    if (isActive) {
      return isActive;
    }
    else {
      return null;
    }

  }



}
