import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-com-tickets',
  templateUrl: './com-tickets.component.html',
  styleUrls: ['./com-tickets.component.scss'],
})
export class ComTicketsComponent implements OnInit {
  public comTickets: any;

  constructor(public httpservice: HttpCallsService,public toastCtrl: ToastController,public router: Router) {
    this.commonAreaTickets();
   }

  ngOnInit() {}
  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  workCompltedVerify(id){

    console.log(id)

    console.log("common area ticket Id " +id)


    this.httpservice.getHttpPost('commonAreaTickets/workCompleteVerify/'+id+'/'+ localStorage.getItem('appUserId')+'').subscribe((data: any) => {
      //  this.indTickets = data;
  //       this.changeSideMenu(data);
  //  this.navigation(data)
  
  // console.log(data)
  // this.comTickets = data
  if(data){
    this.presentToast('Ticked Closed Successfully');
    this.router.navigate(['dashboard']);
  }  
        
       });

  }

  commonAreaTickets(){
    this.httpservice.getHttpPost('commonAreaTickets/appUserComTicket/'+ localStorage.getItem('appUserId')+'').subscribe((data: any) => {
      this.comTickets = data;
//       this.changeSideMenu(data);
//  this.navigation(data)
      
     });
  }

}
