import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InternalVendorIndServceAmountComponent } from './internal-vendor-ind-servce-amount.component';

describe('InternalVendorIndServceAmountComponent', () => {
  let component: InternalVendorIndServceAmountComponent;
  let fixture: ComponentFixture<InternalVendorIndServceAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalVendorIndServceAmountComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InternalVendorIndServceAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
