import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { ToastController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-internal-vendor-ind-servce-amount',
  templateUrl: './internal-vendor-ind-servce-amount.component.html',
  styleUrls: ['./internal-vendor-ind-servce-amount.component.scss'],
})
export class InternalVendorIndServceAmountComponent implements OnInit {
  public vendorServices: any;
  public serviceTypes: any;
  public serviceName: any;
  public serviceInternalAMounts: any = [];
  form: FormGroup = new FormGroup({
    serviceId: new FormControl('', Validators.required),
    serviceTypeId: new FormControl(''),
    amount: new FormControl('', Validators.required),
  })
  constructor(public httpservice: HttpCallsService, public toastController: ToastController, public router: Router,
    public alertController: AlertController) { }

  ngOnInit() {
    this.loadVendroServces();
  }

  loadExtingAmount(serviceId) {

    console.log("ashfdahsfd" + serviceId);
    this.httpservice.getHttpPost('internalVendorServiceTypes/getByServiceId/' + serviceId + '/' + localStorage.getItem("vendorId") + '').subscribe((data: any) => {
      console.log(data);

      console.log("skjdjandajds")

      this.serviceInternalAMounts = data;
      console.log(this.serviceInternalAMounts)

    })

  }

  loadVendroServces() {
    this.httpservice.getHttpPost('vendor/vendorsServices/' + localStorage.getItem("vendorId") + '').subscribe((data: any) => {
      console.log(data);
      this.vendorServices = data;

      // this.presentToast('Work Completed and updated to Vendor ');
      // this.router.navigate(['dashboard']);
      // this.vendorIndTickets = data;
      //       this.changeSideMenu(data);
      //  this.navigation(data)

    });
  }

  onSubmit(data) {
    let obj = {
      serviceTypeId: data.value.serviceTypeId,
      serviceId: data.value.serviceId,
      amount: data.value.amount,
      created_by: localStorage.getItem("appUserId")
    }

    this.httpservice.addHttpPost('internalVendorServiceTypes/add/', obj).subscribe((data: any) => {

      if (data.message == 'Amount Already Added to Selected Service Type') {
        this.presentToast('Amount Already Added to Selected Service Type');
      } else {
        if (data.INTERNAL_VENDOR_AMOUNT_ID) {
          this.form.reset();
          this.presentToast("Amount Added Succesfully");
          this.router.navigate(['vdashboard']);
        } else {
          this.presentToast("Unable to add amount");
        }
      }
    });
  }

  async onEdit(row) {
    console.log(row);
    const id = row.INTERNAL_VENDOR_AMOUNT_ID;
    const amount = row.AMOUNT;
    const alert = await this.alertController.create({
      header: 'Edit Amount!',
      inputs: [
        {
          name: 'amount',
          type: 'text',
          value: amount
          // placeholder: 'Placeholder 1'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log(data.amount);
            const obj = {
              id: id,
              amount: data.amount,
              updated_by: localStorage.getItem('appUserId')
            };

            this.httpservice.addHttpPost('internalVendorServiceTypes/update/', obj).subscribe((data: any) => {
              console.log(data);
              if (data.INTERNAL_VENDOR_AMOUNT_ID) {
                this.presentToast("Amount Updated Succesfully");
                this.router.navigate(['vdashboard']);
              } else {
                this.presentToast("Unable to Update Amount");
              }
            });

            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

  async onDelete($key) {
    console.log($key);
    const alert = await this.alertController.create({
      header: 'Delete!',
      message: '<strong>Are you sure to delete this record ?</strong>!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
            const obj = {
              id: $key
            };
            this.httpservice.delete('internalVendorServiceTypes/delete/', obj).subscribe((data: any) => {
              console.log(data);
              this.router.navigate(['vdashboard']);
              this.presentToast('! Deleted successfully');
            });
          }
        }
      ]
    });

    await alert.present();
  }

 

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
  loadServiceinfo(serviceId) {

    this.httpservice.getHttpPost('indService/getService/' + serviceId + '').subscribe((data: any) => {
      this.serviceName = data.IND_SERVICE_NAME

    })
  }
  getServiceTypes(event) {
    console.log(event)
    console.log(event.detail.value);

    var serviceId = event.detail.value;

    this.loadServiceinfo(serviceId)

    this.httpservice.getHttpPost('indServiceType/getServiceTypes/' + serviceId + '').subscribe((data: any) => {
      console.log(data);
      this.serviceTypes = data;
      this.loadExtingAmount(serviceId);
      // this.presentToast('Work Completed and updated to Vendor ');
      // this.router.navigate(['dashboard']);
      // this.vendorIndTickets = data;
      //       this.changeSideMenu(data);
      //  this.navigation(data)

    });
  }

}
