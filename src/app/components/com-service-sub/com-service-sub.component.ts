import { Component, OnInit } from '@angular/core';
import { ComAreaTIcketService } from '../../services/com-area-ticket.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-com-service-sub',
  templateUrl: './com-service-sub.component.html',
  styleUrls: ['./com-service-sub.component.scss'],
})
export class ComServiceSubComponent implements OnInit {
  public indServicesArray: any = [];
  public mapServSub: any ;
  public planId;
  public imgHeight = 170;
  public indServiceTypes: any =  [];
  public comTicketId: any;
  public serviceName : any;
  public serviceImgName: any;
  public serviceDescription: any;
  public ComticketImage: any;

  constructor(public comTicketService : ComAreaTIcketService, public httpservice: HttpCallsService,public toastController: ToastController,public router: Router) { }

  ngOnInit() {
   this.comTicketId =  this.comTicketService.getServiceiD();
   console.log(this.comTicketId);
   this.getallIndServices();
   this.loadCommonServiceData();
  }
  loadCommonServiceData(){
    this.httpservice.getHttpPost('comService/getComServiceById/'+this.comTicketId+'').subscribe((data: any) => {

      console.log("Loading Com Service")
      console.log(data)
      this.serviceName = data.COM_SERVICE_NAME;
      this.serviceDescription =   data.DESCRIPTION;
      this.serviceImgName =   data.COM_IMAGE_NAME;
    });
  }

  onTicketFileChange(event){

    let files = event.target.files;
    console.log(files);
   // this.imgFilename = files[0].name;
    this.ComticketImage = files[0];
   
   // console.log(files);
   // this.model.files = files;
  
}


getallIndServices(){
  this.httpservice.getHttpPost('indService/allAppIndService/'+localStorage.getItem("associationId")+'').subscribe((data: any) => {
    console.log(data)
    if (data.length > 0) {
      console.log(data.length);
      this.indServicesArray = data;

      console.log(this.indServicesArray)
    }
  });
}


getServiceSubDetails(event){
  var id = event.detail.value;
  this.getMapServcePlans(id);
  this.getIndSerType(id);
}

save(data){
  const formData = new FormData();
  formData.append("description", data.value.ticket_description);
  formData.append("ticket_date", data.value.ticket_date);
  formData.append("ticket_time", data.value.ticket_time);
  formData.append("service_subscription_id", data.value.service_subscription_id);
  formData.append("service_types_id", data.value.service_types_id);
  formData.append("quantity", data.value.quantity);
  
  if (this.ComticketImage) {
    formData.append("ComticketImage", this.ComticketImage);
  }
  formData.append("associationId", localStorage.getItem("associationId"));
  formData.append("appUserId", localStorage.getItem("appUserId"));
  formData.append("comServiceId", this.comTicketId);
  formData.append("serviceId", data.value.ind_service_id);
  formData.append("created_by", localStorage.getItem("appUserId"));

  var serviceTypeId = data.value.service_types_id;
var quantity = data.value.quantity;
  this.httpservice.addHttpPost('commonAreaTickets/addCommonTicket', formData).subscribe((data: any) => {
    console.log(data);
  
    if(data.COM_TICKET_ID){
 
      this.comTicketService.form.reset();
     // this.comTicketService.initializeFormGroup();
      // this.comTicketService.initializeFormGroup(serviceTypeId,quantity);
      // this.presentToast("Ticket Raised Sucessfully");
      // this.router.navigate(['dashboard']);

      this.presentToast("Ticket Raised Sucessfully");
      setTimeout(() => {
        window.location.reload();
    }, 3000);
    }
    else {
      this.presentToast("Unable to raise ticket please try again later");
    }
  
  
  })
}


checkUnitWise(event)
{
  var value = event.detail.value;
   if( value == '2'){
     this.imgHeight = 120;
     this.comTicketService.form.addControl('quantity', new FormControl('', Validators.required));
     this.planId  = value;
   }
 }

 async presentToast(msg) {
  const toast = await this.toastController.create({
    message: msg,
    duration: 2000
  });
  toast.present();
}


getMapServcePlans(id){
  this.httpservice.getHttpPost('indSerPlan/getPlansByService/'+id+'').subscribe((data: any) => {
    console.log(data);
    this.mapServSub =  data;
    console.log(this.mapServSub)
  });
}

getIndSerType(id){
  this.httpservice.getHttpPost('indServiceType/getServiceTypes/'+id+'').subscribe((data: any) => {
    if(data.length > 0){

      this.indServiceTypes = data;
      this.comTicketService.form.addControl('service_types_id', new FormControl('', Validators.required));
    }
    else {
      this.comTicketService.form.removeControl('service_types_id');
    }
  })
}

}
