import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ComServiceSubComponent } from './com-service-sub.component';

describe('ComServiceSubComponent', () => {
  let component: ComServiceSubComponent;
  let fixture: ComponentFixture<ComServiceSubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComServiceSubComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ComServiceSubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
