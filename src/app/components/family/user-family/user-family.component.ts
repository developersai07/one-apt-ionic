import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AddFamilyMembersModelPage } from '../add-family-members-model/add-family-members-model.page';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { UserFamilyService } from 'src/app/services/user-family.service';

@Component({
  selector: 'app-user-family',
  templateUrl: './user-family.component.html',
  styleUrls: ['./user-family.component.scss'],
})
export class UserFamilyComponent implements OnInit {

  public relationsObj: any = [];
  constructor(public modalController: ModalController, public httpservice: HttpCallsService, public service: UserFamilyService) { }

  ngOnInit() {
    this.httpservice.getHttpPost('associationFamilyRelations/getAllAppAssociationFamilyRelations').subscribe((data: any) => {
      console.log(data);
      this.relationsObj = data;
    });
  }

  async openModel() {
    const modal = await this.modalController.create({
      component: AddFamilyMembersModelPage
    });
    modal.onDidDismiss().then((dataReturned) => {
      console.log('dismiss Modal');
    });
    return await modal.present();
  }

  addFamily() {
    this.openModel();
  }

  takeAction(relationList) {
    console.log(relationList);
    this.service.editForm(relationList);
    this.openModel();
  }
}
