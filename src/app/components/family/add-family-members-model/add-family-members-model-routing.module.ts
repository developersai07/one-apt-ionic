import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AddFamilyMembersModelPage } from './add-family-members-model.page';

const routes: Routes = [
  {
    path: '',
    component: AddFamilyMembersModelPage
  }
];

@NgModule({
  imports: [FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddFamilyMembersModelPageRoutingModule {}
