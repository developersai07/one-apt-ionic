import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AddFamilyMembersModelPage } from './add-family-members-model.page';

describe('AddFamilyMembersModelPage', () => {
  let component: AddFamilyMembersModelPage;
  let fixture: ComponentFixture<AddFamilyMembersModelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFamilyMembersModelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddFamilyMembersModelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
