import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { UserFamilyService } from 'src/app/services/user-family.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-family-members-model',
  templateUrl: './add-family-members-model.page.html',
  styleUrls: ['./add-family-members-model.page.scss'],
})
export class AddFamilyMembersModelPage implements OnInit {
  public relationsObj: any;
  public associationObj: any = [];
  public flatOwnerObj: any = [];
  public associationName: any;

  constructor(private modalController: ModalController, public service: UserFamilyService, public httpservice: HttpCallsService,
              public toastController: ToastController, public router: Router) {
    this.getfamilyRelationType();
    this.getAssociation();
    // this.getFlatOwner();
  }

  ngOnInit() {
  }

  getfamilyRelationType() {
    this.httpservice.getHttpPost('familyRelations/familyRelationAppGetAll').subscribe((data: any) => {
      console.log(data);
      this.relationsObj = data;
    });
  }
  getAssociation() {
    this.httpservice.getHttpPost('association/associationInformation/' + localStorage.getItem('associationId') + '')
      .subscribe((data: any) => {
        console.log(data);
        this.associationObj = data;
        // const associatoionName = this.associationObj.ASSOCIATION_NAME;
        // const associationName = data.ASSOCIATION_NAME;
        // this.associationName = associationName;
        // console.log(this.associationName);
        console.log(this.associationObj);
      });
  }

  getFlatOwner() {
    this.httpservice.getHttpPost('flatOwner/flatOwnerInformation' + localStorage.getItem('flatOwnerId') + '')
      .subscribe((data: any) => {
        console.log(data);
        this.flatOwnerObj = data;
        console.log(this.flatOwnerObj);
      });
  }

  familyDetails(value) {
    console.log(value);
    if (value.id) {
      console.log('update');
      let obj = {
        id: value.id,
        relative_name: value.relative_name,
        age: value.age,
        phone_number: value.phone_number,
        email_id: value.email_id,
        relation_id: value.relation_id,
        app_user_id: localStorage.getItem('appUserId'),
        association_id: localStorage.getItem('associationId'),
        updated_by: localStorage.getItem('appUserId'),
      };
      console.log(obj);
      this.httpservice.addHttpPost('associationFamilyRelations/updateAppAssociationFamilyRelations', obj).subscribe((res: any) => {
        console.log(res);
        if (res.ID) {
          this.modalClose();
          this.presentToast('Family Member Updated Sucessfully');
        } else {
          this.presentToast('Unable to update Family Member Entity.');
        }
      });

    } else {
      console.log('Add');
      let obj = {
        relative_name: value.relative_name,
        age: value.age,
        phone_number: value.phone_number,
        email_id: value.email_id,
        relation_id: value.relation_id,
        app_user_id: localStorage.getItem('appUserId'),
        association_id: localStorage.getItem('associationId'),
        created_by: localStorage.getItem('appUserId'),
        updated_by: localStorage.getItem('appUserId'),
      };
      console.log(obj);
      this.httpservice.addHttpPost('associationFamilyRelations/addAppAssociationFamilyRelations', obj).subscribe((res: any) => {
        console.log(res);
        if (res.ID) {
          this.modalClose();
          this.presentToast('Family Member Added Sucessfully');
        } else {
          this.presentToast('Unable to add Family Member Entity.');
        }
      });
    }
  }


  modalClose() {
    this.service.emptyFormGroup();
    this.modalController.dismiss();
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
