import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddFamilyMembersModelPageRoutingModule } from './add-family-members-model-routing.module';

import { AddFamilyMembersModelPage } from './add-family-members-model.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AddFamilyMembersModelPageRoutingModule
  ],
  declarations: [AddFamilyMembersModelPage]
})
export class AddFamilyMembersModelPageModule {}
