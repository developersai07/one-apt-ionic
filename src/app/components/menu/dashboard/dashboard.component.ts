import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides, NavController } from '@ionic/angular';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { IndServiceService } from '../../../services/ind-service.service';
import { ComAreaTIcketService } from '../../../services/com-area-ticket.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @ViewChild('slideWithNav', { static: true }) slideWithNav: IonSlides;
  sliderOne: any;
  sliderTwo: any;
  sliderThree: any;
  comServicesArray: any;
  indServicesArray: any;
  advertisementArray: any;
  segment: string;


  // Configuration for each Slider
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true,
    speed: 200,
  };

  constructor(public router: Router, public httpservice: HttpCallsService, public navCtrl: NavController,
    public indService: IndServiceService, public comTicketService: ComAreaTIcketService) {
    this.getAllAdvertisement();
    this.sliderOne = {
      isBeginningSlide: true,
      isEndSlide: false,
      slidesItems: [
        {
          id: 1,
          image: '../../assets/images/1.jpg'
        },
        {
          id: 2,
          image: '../../assets/images/2.jpg'
        },
        {
          id: 3,
          image: '../../assets/images/3.jpg'
        },
        {
          id: 4,
          image: '../../assets/images/4.jpg'
        },
        {
          id: 5,
          image: '../../assets/images/5.jpg'
        }
      ]
    };
  }
  //Method called when slide is changed by drag or navigation
  SlideDidChange(object, slideView) {
    this.checkIfNavDisabled(object, slideView);
  }

  viewIndService(serviceId) {
    this.indService.setServiceId(serviceId);

    this.router.navigate(['indServiceSub']);
  }

  viewComService(serviceId) {
    console.log(serviceId);
    this.router.navigate(['comServiceRaise']);
    this.comTicketService.setServiceId(serviceId)

  }

  //Move to previous slide
  slidePrev(object, slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }

  //Move to Next slide
  slideNext(object, slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }

  //Call methods to check if slide is first or last to enable disbale navigation  
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }

  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }

  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }

  ngOnInit() {
    console.log("Dash2")
    this.getAllComService();
    this.getAllIndService();
    this.segment = 'Individual';
  }

  ionViewWillEnter() {

    console.log("Dash1")
    this.ngOnInit()
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['']);
  }

  getAllComService() {
    this.httpservice.getHttpPost('comService/allAppComService/' + localStorage.getItem("associationId") + '').subscribe((data: any) => {
      if (data.length > 0) {

        this.comServicesArray = data;
      }
    });

  }

  updateList(ev) {
    if (ev.target.value.length > 0) {
      this.getindSearchServices(ev.target.value);
      this.getComSearchServices(ev.target.value);
    }
    else {
      this.getAllComService();
      this.getAllIndService();
    }
  }

  getindSearchServices(string) {
    this.httpservice.getHttpPost('indService/searchService/' + string + '').subscribe((data: any) => {
      if (data.length > 0) {
        console.log(data.length);
        this.indServicesArray = data;
      }
    });
  }

  getComSearchServices(string) {
    this.httpservice.getHttpPost('comService/searchService/' + string + '').subscribe((data: any) => {
      if (data.length > 0) {
        console.log(data.length);
        this.comServicesArray = data;
      }
    });
  }

  getAllAdvertisement() {
    this.httpservice.getHttpPost('advertisement/getAppAllAdvertisement').subscribe((data: any) => {
      console.log(data);
      this.advertisementArray = data;
      this.sliderOne = {
        isBeginningSlide: true,
        isEndSlide: false,
        slidesItems: data
      };

    });
  }

  getAllIndService() {
    this.httpservice.getHttpPost('indService/allAppIndService/' + localStorage.getItem("associationId") + '').subscribe((data: any) => {
      console.log(data)
      if (data.length > 0) {
        console.log(data.length);
        this.indServicesArray = data;
      }
    });

  }


}
