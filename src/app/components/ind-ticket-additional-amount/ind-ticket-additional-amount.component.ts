import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { HttpCallsService } from 'src/app/common/http-service.service';
import { IndTicketAdditionalAmountService } from 'src/app/services/ind-ticket-additional-amount.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-ind-ticket-additional-amount',
  templateUrl: './ind-ticket-additional-amount.component.html',
  styleUrls: ['./ind-ticket-additional-amount.component.scss'],
})
export class IndTicketAdditionalAmountComponent implements OnInit {

  form: FormGroup = new FormGroup({
    amount: new FormControl('', Validators.required),
    Reason: new FormControl('', Validators.required),
  
  });

  constructor(public httpservice: HttpCallsService,public indTicketAddAmount: IndTicketAdditionalAmountService,public toastController: ToastController,public router: Router, ) { }

  ngOnInit() {
   
  }

  save(data){

    if(this.indTicketAddAmount.getTicket() == 'IND'){
      let obj = {
        amount : data.value.amount,
        reason : data.value.Reason,
        indTicketId :  this.indTicketAddAmount.getIndTicketId(),
        created_by: localStorage.getItem("appUserId"),
        vendorId : localStorage.getItem("vendorId") 
      }
      console.log(obj);
      this.httpservice.addHttpPost('indTicketAdditionalAmount/add', obj).subscribe((data: any) => {
      
        if(data.IND_TICKETS_ADDITIONAL_AMOUNT_ID){
          this.presentToast("Ticket Amount Added Sucessfully");
          this.router.navigate(['dashboard']);
        }
        else {
          this.presentToast("Unable to add Ticket Amount");
        }
      
      })
    }
    else {

      let obj = {
        amount : data.value.amount,
        reason : data.value.Reason,
        comTicketId :  this.indTicketAddAmount.getComTIcketId(),
        created_by: localStorage.getItem("appUserId"),
        vendorId : localStorage.getItem("vendorId") 
      }
      console.log(obj);
      this.httpservice.addHttpPost('comTicketAdditionalAmount/add', obj).subscribe((data: any) => {
      
        if(data.COM_TICKETS_ADDITIONAL_AMOUNT_ID){
          this.presentToast("Ticket Amount Added Sucessfully");
          this.router.navigate(['dashboard']);
        }
        else {
          this.presentToast("Unable to add Ticket Amount");
        }
      
      })

    }

  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
