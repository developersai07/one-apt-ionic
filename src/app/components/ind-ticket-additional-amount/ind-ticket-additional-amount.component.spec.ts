import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IndTicketAdditionalAmountComponent } from './ind-ticket-additional-amount.component';

describe('IndTicketAdditionalAmountComponent', () => {
  let component: IndTicketAdditionalAmountComponent;
  let fixture: ComponentFixture<IndTicketAdditionalAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndTicketAdditionalAmountComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IndTicketAdditionalAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
