import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/services/profile.service';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  public appartmentChildEntities: any;
  public appartmentGrandChildEntities: any;
  public ownerprofileImage: any;
  public ImageName: any;
  // fileUrl: any = null;
  // respData: any;
  public flatOwnerId: string;
  public flatTenantId: string;
  public vendorId: string;
  // public user_Data: any;
  public flatOwnerObj: any = [];
  public flatTenantObj: any = [];
  public vendorObj: any = [];
  role: any = [];
  flatOwner: boolean;
  flatTenant: boolean;
  vendor: boolean;
  constructor(public profileService: ProfileService, public httpservice: HttpCallsService, public router: Router,
    public toastCtrl: ToastController) {
    this.flatOwnerId = localStorage.getItem('flatOwnerId');
    this.flatTenantId = localStorage.getItem('flatTenantId');
    this.vendorId = localStorage.getItem('vendorId');


  }

  ngOnInit() {

    if (localStorage.getItem('flatOwnerId')) {
      this.flatOwnerById();
    }
    if (localStorage.getItem('flatTenantId')) {
      this.flatTenantById();
    }
    if (localStorage.getItem('vendorId')) {
      this.vendorById();
    }


    this.loadRoles();

    console.log("this.imagename")
    console.log(this.ImageName);

  }




  onProfileFileChange(event) {

    let files = event.target.files;
    console.log(files);
    // this.imgFilename = files[0].name;
    this.ownerprofileImage = files[0];

    // console.log(files);
    // this.model.files = files;

  }

  flatOwnerById() {
    this.httpservice.getHttpPost('flatOwners/getById/' + this.flatOwnerId + '').subscribe((data: any) => {
      this.flatOwnerObj.push(data);
      this.profileService.populateForm(data);
      this.ImageName = this.profileService.getuser_IMAGE_NAME();
    });
  }

  flatTenantById() {
    this.httpservice.getHttpPost('flatTenants/getById/' + this.flatTenantId + '').subscribe((data: any) => {
      this.flatTenantObj.push(data);
      this.profileService.populateTenantForm(data);
      this.ImageName = this.profileService.getuser_IMAGE_NAME();
    });
  }

  vendorById() {
    this.httpservice.getHttpPost('vendor/getById/' + this.vendorId + '').subscribe((data: any) => {
      this.vendorObj.push(data);
      this.profileService.populateVendorForm(data);
      this.ImageName = this.profileService.getuser_IMAGE_NAME();
    });
  }



  loadRoles() {
    this.httpservice.getHttpPost('Admin/roles/' + localStorage.getItem('appUserId') + '').subscribe((data: any) => {
      // console.log(data);
      this.navigation(data);
    });
  }

  navigation(roles) {
    var flatOwnerRole = this.checkFlatOwner(roles);
    var flatTenantRole = this.checkFlatTenant(roles);
    var vendorRole = this.checkVendor(roles);

    if (flatOwnerRole) {
      this.flatOwner = flatOwnerRole;
    } else if (flatTenantRole) {
      this.flatTenant = flatTenantRole;
    } else if (vendorRole) {
      this.vendor = vendorRole;
    }
  }

  checkFlatOwner(roles) {
    var isFlatOwner = roles.find(x => x.role.ROLE_NAME === 'FLAT OWNER');
    if (isFlatOwner) {
      return true
    } else {
      return false;
    }
  }

  checkFlatTenant(roles) {
    var isFlatTenant = roles.find(x => x.role.ROLE_NAME === 'FLAT TENANT');
    if (isFlatTenant) {
      return true;
    } else {
      return false;
    }
  }

  checkVendor(roles) {
    var isVendor = roles.find(x => x.role.ROLE_NAME === 'VENDOR');
    if (isVendor) {
      return true;
    } else {
      return false;
    }
  }

  onOwnerSubmit(value: any) {
    // console.log(value);
    const ownerData = new FormData();
    ownerData.append('id', value.id);
    ownerData.append('first_name', value.first_name);
    ownerData.append('last_name', value.last_name);
    ownerData.append('email', value.email);
    ownerData.append('mobile_number', value.mobile_number);
    ownerData.append('owner_pic', this.ownerprofileImage);
    // ownerData.append('address', value.address);
    // ownerData.append('appartmentId', localStorage.getItem('appartmentId'));
    // ownerData.append('associationId', localStorage.getItem('associationId'));
    // ownerData.append('updated_by', localStorage.getItem('appUserId'));

    // let data = {
    //   id: value.id,
    //   first_name: value.first_name,
    //   last_name: value.last_name,
    //   email: value.email.toLowerCase(),
    //   mobile_number: value.mobile_number,
    //   // address: value.address,
    //   // appartmentId: localStorage.getItem('appartmentId'),
    //   associationId: localStorage.getItem('associationId'),
    //   updated_by: localStorage.getItem('appUserId'),
    // };
    // console.log(data);
    this.httpservice.addHttpPost('flatOwners/appUpdate', ownerData).subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.presentToast('Updated Successfully');
        window.location.reload();
        // if(this.ownerprofileImage){

        //   window.location.reload();

        // }
        // else {
        //   this.router.navigate(['/dashboard']);
        //  // this.presentToast('Updated Successfully');
        // }

      } else {
        this.presentToast('Not updated');
      }
    });
  }

  onTenantSubmit(value) {
    console.log(value);
    const tenantData = new FormData();
    tenantData.append('id', value.id);
    tenantData.append('first_name', value.first_name);
    tenantData.append('last_name', value.last_name);
    tenantData.append('email', value.email);
    tenantData.append('mobile_number', value.mobile_number);
    tenantData.append('tenant_pic', this.ownerprofileImage);
    tenantData.append('address', value.address);
    tenantData.append('appartmentId', localStorage.getItem('appartmentId'));
    tenantData.append('associationId', localStorage.getItem('associationId'));
    tenantData.append('updated_by', localStorage.getItem('appUserId'));
    // let data = {
    //   id: value.id,
    //   first_name: value.first_name,
    //   last_name: value.last_name,
    //   email: value.email.toLowerCase(),
    //   mobile_number: value.mobile_number,
    //   // address: value.address,
    //   // appartmentId: localStorage.getItem('appartmentId'),
    //   associationId: localStorage.getItem('associationId'),
    //   updated_by: localStorage.getItem('appUserId'),
    // };

    this.httpservice.addHttpPost('flatTenants/appUpdate', tenantData).subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.presentToast('Updated Successfully');
        window.location.reload();
        // if(this.ownerprofileImage){

        //   window.location.reload();

        // }
        // else {
        //   this.router.navigate(['/dashboard']);
        //  // this.presentToast('Updated Successfully');
        // }
        // this.presentToast('Updated Successfully');
      } else {
        this.presentToast('Not updated');
      }
    });
  }

  onVendorSubmit(value) {
    // console.log(value);
    const vendorData = new FormData();
    vendorData.append('id', value.id);
    vendorData.append('first_name', value.name);
    vendorData.append('email', value.email);
    vendorData.append('mobile_number', value.mobile_number);
    vendorData.append('vendor_pic', this.ownerprofileImage);
    vendorData.append('address', value.address);
    vendorData.append('associationId', localStorage.getItem('associationId'));
    vendorData.append('updated_by', localStorage.getItem('appUserId'));
    // let data = {
    //   id: value.id,
    //   name: value.name,
    //   email: value.email.toLowerCase(),
    //   mobile_number: value.mobile_number,
    //   // address: value.address,
    //   updated_by: localStorage.getItem('appUserId'),
    // };

    this.httpservice.addHttpPost('vendor/updateAppVendor', vendorData).subscribe((res: any) => {
      console.log(res);
      if (res) {
        this.presentToast('Updated Successfully');
        window.location.reload();
        // if(this.ownerprofileImage){

        //   window.location.reload();

        // }
        // else {
        //  // this.router.navigate(['/vdashboard']);
        //  // this.presentToast('Updated Successfully');
        // }

      }
    });

  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
