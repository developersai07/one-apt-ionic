import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { IndTicketAdditionalAmountService } from '../../../services/ind-ticket-additional-amount.service';
@Component({
  selector: 'app-vendor-ind-tickets',
  templateUrl: './vendor-ind-tickets.component.html',
  styleUrls: ['./vendor-ind-tickets.component.scss'],
})
export class VendorIndTicketsComponent implements OnInit {
  public vendorIndTickets: any;
  public vendorIndTicketsCompleted: any = [];
  segment: string;
  constructor(public indTicketAdiitionalAmountService : IndTicketAdditionalAmountService,public httpservice: HttpCallsService,public alertController: AlertController,public router: Router,   public toastCtrl: ToastController, ) {
    this.loadVendorTickets();
    this.loadVendorCompletedTickets();
    this.segment = 'Ind_Pending';

   }
   loadVendorCompletedTickets(){
    this.httpservice.getHttpPost('indTickets/vendorIndividualTicketsCompleted/'+ localStorage.getItem('vendorId')+'').subscribe((data: any) => {
      console.log(data);
      this.vendorIndTicketsCompleted = data;
//       this.changeSideMenu(data);
//  this.navigation(data)
      
     });
   }
   loadVendorTickets(){
    this.httpservice.getHttpPost('indTickets/vendorIndividualTickets/'+ localStorage.getItem('vendorId')+'').subscribe((data: any) => {
      console.log(data);
      this.vendorIndTickets = data;
//       this.changeSideMenu(data);
//  this.navigation(data)
      
     });
   }

   
checkIsActive(data){
  var isActive = data.find(x => x.IS_ACTIVE == true && x.IS_VERIFIED == true);
if(isActive)
{
return isActive;
}
else 
{
  return null;
}

}


otpVerified(data){
  var ticketOtpArray = [];
  ticketOtpArray = data;
  var ticketOtp;
  if(ticketOtpArray.length > 0){
    for(var i = 0;i< ticketOtpArray.length;i++ ){
      ticketOtp  = ticketOtpArray[i];
    var checkres = this.comapreDate(ticketOtp.OTP_DATE);
  
    if(checkres == true){
if(ticketOtp.IS_VERIFIED == true){


  console.log("Otp True");
  console.log(ticketOtp.IS_VERIFIED+"+ticketOtp");
return true;
}
    }
    }
  }
  else {
    return null
  }

}

otpVerifiedWorkCompleted(data){
  var ticketOtpArray = [];
  ticketOtpArray = data;
  var ticketOtp;
  if(ticketOtpArray.length > 0){
    for(var i = 0;i< ticketOtpArray.length;i++ ){
      ticketOtp  = ticketOtpArray[i];
    var checkres = this.comapreDate(ticketOtp.OTP_DATE);
  
    if(checkres == true){
if(ticketOtp.IS_VERIFIED == true && ticketOtp.IS_WORK_COMPLETED == false){


  console.log("Otp True");
  console.log(ticketOtp.IS_VERIFIED+"+ticketOtp");
return true;
}
    }
    }
  }
  else {
    return null
  }

}

otpVerifiedWorkCompletedStatus(data){
  var ticketOtpArray = [];
  ticketOtpArray = data;
  var ticketOtp;
  if(ticketOtpArray.length > 0){
    for(var i = 0;i< ticketOtpArray.length;i++ ){
      ticketOtp  = ticketOtpArray[i];
    var checkres = this.comapreDate(ticketOtp.OTP_DATE);
  
    if(checkres == true){
if(ticketOtp.IS_WORK_COMPLETED == true){


  console.log("Otp True");
  console.log(ticketOtp.IS_VERIFIED+"+ticketOtp");
return true;
}
    }
    }
  }
  else {
    return false
  }
}

workComplted(id){
  var cr_date = new Date();
  var cur_date = cr_date.getDate();
var cur_month = cr_date.getMonth();
var cur_year = cr_date.getFullYear();
cur_month = cur_month +1;
var formattedDate = cur_year+'-'+cur_month+'-'+cur_date;
  let obj = {
    indTicketId : id,
    otpDate : formattedDate
  }
this.httpservice.addHttpPost('indTickets/vendorWorkCompleted',obj).subscribe((data: any) => {
  console.log(data);
  this.presentToast('Work Completed and updated to Owner ');
  this.router.navigate(['vdashboard']);
 // this.vendorIndTickets = data;
//       this.changeSideMenu(data);
//  this.navigation(data)
  
 });
}

addAmount(id){
  this.indTicketAdiitionalAmountService.setIndTicketId(id);
  this.indTicketAdiitionalAmountService.setTicket('IND');

  //addAmount
  this.router.navigate(['indAddtionalTicketAmount']);
}
      comapreDate(date){
    
        var otpDate = new Date(date);
    var cr_date = new Date();
    var otp_date = otpDate.getDate();
    var otp_month = otpDate.getMonth();
    var otp_year = otpDate.getFullYear();
    var cur_date = cr_date.getDate();
    var cur_month = cr_date.getMonth();
    var cur_year = cr_date.getFullYear();
    cur_month = cur_month +1;
    otp_month = otp_month +1;
   
    
    var formattedDate = cur_year+'-'+cur_month+'-'+cur_date;
    var oDate = otp_year+'-'+otp_month+'-'+otp_date;
    
  
    if(formattedDate == oDate){
      return true
    }
    else {
      return null;
    }
    
      }

   async startWork(id) {

    const alert = await this.alertController.create({
      header: 'Enter Otp!',
      inputs: [
        {
          name: 'otp',
          type: 'text',
          placeholder: 'Enter Otp'
        },
       
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {

            var cr_date = new Date();
            var cur_date = cr_date.getDate();
var cur_month = cr_date.getMonth();
var cur_year = cr_date.getFullYear();
cur_month = cur_month +1;
var formattedDate = cur_year+'-'+cur_month+'-'+cur_date;
 
         let obj = {
          ticketid : id,
          vendorID : localStorage.getItem('vendorId'),
          otpNumber  : data.otp,
          ticketDate : formattedDate
         }
         this.httpservice.addHttpPost('indTickets/indTicketsOtpVerify',obj).subscribe((data: any) => {
         if(data == true){
          this.presentToast('Otp Verified Sucessfully');
          this.router.navigate(['vdashboard']);
         }
         else if(data == null){
          this.presentToast('Invalid Otp');
         
         }
          
         });

          }
        }
      ]
    });

    await alert.present();

   }

   accept(id){
    this.httpservice.getHttpPost('indTickets/vendorIndActionAccept/'+id+'/'+localStorage.getItem('vendorId')+'').subscribe((data: any) => {
      console.log(data);
if(data.id){
  this.presentToast('Accepted ticket');
  this.router.navigate(['vdashboard']);
}
else {
  this.presentToast('Unable Accepted ticket. Please try again after some time');  
}
      
     });

   }
   async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
   reject(id,ticketDate,ticketTime,associationId,serviceId ){
    // this.httpservice.getHttpPost('indTickets/vendorIndActionReject/'+id+'/'+localStorage.getItem('vendorId')+'').subscribe((data: any) => {
    //   if(data.id){
    //     this.presentToast('Rejected ticket');
    //     this.router.navigate(['vdashboard']);
    //   }
    //   else {
    //     this.presentToast('Unable Reject ticket. Please try again after some time');  
    //   }

      
    //  });
    let obj = {
      indTicketId : id,
      vendorId:localStorage.getItem('vendorId'),
      ticket_date : ticketDate,
      ticket_time : ticketTime,
      serviceId : serviceId,
      associationId: associationId
    }
       this.httpservice.addHttpPost('indTickets/vendorIndActionRejectAutoAdding',obj).subscribe((data: any) => {
      if(data){
        this.presentToast('Rejected ticket');
        this.router.navigate(['vdashboard']);
      }
      else {
        this.presentToast('Unable Reject ticket. Please try again after some time');  
      }

      
     });
       }

  ngOnInit() {}

 

}
