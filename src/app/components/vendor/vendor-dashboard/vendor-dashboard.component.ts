import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { NgAnalyzeModulesHost } from '@angular/compiler';
import { Router } from '@angular/router';
@Component({
  selector: 'app-vendor-dashboard',
  templateUrl: './vendor-dashboard.component.html',
  styleUrls: ['./vendor-dashboard.component.scss'],
})
export class VendorDashboardComponent implements OnInit {
  doughnutChartLabels = ['Pending', 'Completed'];
  doughnutChartData = [350, 450];
  doughnutChartType = 'doughnut';
  donutOptions: any = {
    legend: {
      display: true,
      position: 'right'
    }
  };
  doughnutChartCommonLabels = ['Pending', 'Completed'];
  doughnutChartCommonData = [350, 450];
  doughnutChartCommonType = 'doughnut';
  donutCommonOptions: any = {
    legend: {
      display: true,
      position: 'right'
    }
  };
  public indCompletedCount: any;
  public indPendingCount: any;

  public comPendingCount: any;
  public comCompletedCount: any;
  public String: any;
  public ind: boolean;
  public com: boolean;

  constructor(public httpservice: HttpCallsService, public router: Router) {
    this.String = "Individual Area";
    this.ind = true;
    this.com = false;
  }
  vendorIndividualTickets
  ngOnInit() {
    this.loadVendorIndTicketsCount();
    this.loadVendorComTIcketsCount()


  }

  ionViewWillEnter() {
    this.ngOnInit()
  }


  naviagteInd() {
    this.router.navigate(['vendorIndividualTickets']);
  }

  naviagteCom() {
    this.router.navigate(['vendorCommonTickets']);

  }
  loadVendorIndTicketsCount() {
    this.httpservice.getHttpPost('indTickets/vendorIndividualTicketsPendingCompleted/' + localStorage.getItem('vendorId') + '').subscribe((data: any) => {
      console.log(data);
      this.indPendingCount = data[0];
      this.indCompletedCount = data[1];
      this.doughnutChartData = data;

    });
  }
  loadVendorComTIcketsCount() {
    this.httpservice.getHttpPost('commonAreaTickets/appVendorPendingComTicket/' + localStorage.getItem('vendorId') + '').subscribe((data: any) => {
      console.log(data);
      this.comPendingCount = data[0];
      this.comCompletedCount = data[1];
      this.doughnutChartCommonData = data;
    });
  }
  checkData(event) {
    var value = event.detail.value;

    if (value == "Ind") {
      this.String = "Individual Area";
      this.ind = true;
      this.com = false;
    }
    if (value == "Com") {
      this.String = "Common Area";
      this.ind = false;
      this.com = true;

    }
  }

}
