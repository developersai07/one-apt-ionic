import { Component, OnInit } from '@angular/core';
import { HttpCallsService } from 'src/app/common/http-service.service';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { IndTicketAdditionalAmountService } from '../../../services/ind-ticket-additional-amount.service';
@Component({
  selector: 'app-vendor-com-tickets',
  templateUrl: './vendor-com-tickets.component.html',
  styleUrls: ['./vendor-com-tickets.component.scss'],
})
export class VendorComTicketsComponent implements OnInit {
  public vendorComTickets: any;
  public vendorComPletedTickets: any;
  segment: string;
  constructor(public indTicketAdiitionalAmountService : IndTicketAdditionalAmountService,public httpservice: HttpCallsService,public router: Router,   public toastCtrl: ToastController) { }

  ngOnInit() {
    this.loadVendorCommonTickets();
    this.loadVendorCommonTicketsCompletd()
    this.segment = 'Com_Pending';
  }

  loadVendorCommonTickets(){
    this.httpservice.getHttpPost('commonAreaTickets/appVendorComTicket/'+ localStorage.getItem('vendorId')+'').subscribe((data: any) => {
      console.log(data);
      this.vendorComTickets = data;
//       this.changeSideMenu(data);
//  this.navigation(data)
      
     });
   }

   loadVendorCommonTicketsCompletd(){
    this.httpservice.getHttpPost('commonAreaTickets/appVendorComTicketCompleted/'+ localStorage.getItem('vendorId')+'').subscribe((data: any) => {
      console.log(data);
      this.vendorComPletedTickets = data;
//       this.changeSideMenu(data);
//  this.navigation(data)
      
     });
   }

   accept(id){

    let obj = {
      id : id,
      status : '1'
    }
    this.httpservice.addHttpPost('commonAreaTickets/mapCommonVendorAction',obj).subscribe((data: any) => {
      console.log(data);
    //  this.vendorComTickets = data;
//       this.changeSideMenu(data);
//  this.navigation(data)
this.presentToast('Ticket Accepted Successfully ');
this.router.navigate(['vdashboard']);
      
     });

   }

    startWork(id) {

      this.httpservice.getHttpPost('commonAreaTickets/mapAssociationComTicketAction/'+id+'/workStarted').subscribe((data: any) => {
        console.log(data);
  //       this.vendorComTickets = data;
  // //       this.changeSideMenu(data);
  // //  this.navigation(data)
  this.presentToast('Work Started Successfully ');
  this.router.navigate(['vdashboard']);
  
        
       });

   }

   completeWork(id) {

    this.httpservice.getHttpPost('commonAreaTickets/mapAssociationComTicketAction/'+id+'/workCompleted').subscribe((data: any) => {
      console.log(data);
//       this.vendorComTickets = data;
// //       this.changeSideMenu(data);
// //  this.navigation(data)
this.presentToast('Work Completed and updated to Owner ');
this.router.navigate(['vdashboard']);

      
     });

 }

   async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
   reject(id,comTicketId,ticketDate,ticketTime,associationId,indServiceId,serviceTypeId){
    let obj = {
      id : id,
      status : '2',
      ticket_date : ticketDate,
      ticket_time : ticketTime,
      associationId : associationId,
      serviceId : indServiceId,
      comTicketId : comTicketId,
      service_types_id : serviceTypeId

    }

    console.log(obj)
    this.httpservice.addHttpPost('commonAreaTickets/mapCommonVendorAction',obj).subscribe((data: any) => {
      console.log(data);
      this.presentToast('Ticket Rejected Successfully ');
      this.router.navigate(['vdashboard']);
      
     });

   }
   addAmount(id){
    this.indTicketAdiitionalAmountService.setComTicketId(id);
    this.indTicketAdiitionalAmountService.setTicket('COM');
  
    //addAmount
    this.router.navigate(['indAddtionalTicketAmount']);
  }


}
