import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { STEP_ITEMS } from '../../constants/multi-step-form';
import { HttpCallsService } from '../common/http-service.service';

import { AuthService } from '../providers/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  formContent: any;
  formData: any;
  activeStepIndex: number;
  public onLoginForm: FormGroup;
  form: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  })


  constructor(private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public httpservice: HttpCallsService,
    public router: Router,
    public authService: AuthService
  ) { }

  ngOnInit() {

  }

  async forgotPass() {
    const alert = await this.alertCtrl.create({
      header: 'Forgot Password?',
      message: 'Enter you email address to send a reset link password.',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: async () => {
            const loader = await this.loadingCtrl.create({
              duration: 2000
            });

            loader.present();
            loader.onWillDismiss().then(async l => {
              const toast = await this.toastCtrl.create({
                // showCloseButton: true,
                message: 'Email was sended successfully.',
                duration: 3000,
                position: 'bottom'
              });

              toast.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }
  onSubmit(form) {
    console.log(form.value);
    const data = form.value;
    console.log(data);
    this.httpservice.addHttpPost('appUser/application/login', data).subscribe((data: any) => {
      console.log(data.user);
      if (data.user) {
        localStorage.setItem('appUserId', data.user.APP_USER_ID);
        localStorage.setItem('jwt', data.jwtToken);
        if (data.user.flatOwner) {
          localStorage.setItem('flatOwnerId', data.user.flatOwner.FLAT_OWNER_ID);
          // localStorage.setItem('appartmentId', data.user.flatOwner.appartmentEntity.id);
          localStorage.setItem('associationId', data.user.flatOwner.associationEntity.id);
          localStorage.setItem('loginUserName', data.user.flatOwner.OWNER_FIRST_NAME );
          this.router.navigate(['dashboard']);
          this.authService.isUserLoggedIn.next(true);
          this.presentToast('Login Sucessfull');
        } else if (data.user.flatTenant) {
          localStorage.setItem('associationId', data.user.flatTenant.associationEntity.id);
          localStorage.setItem('flatTenantId', data.user.flatTenant.TENANT_ID);
          localStorage.setItem('loginUserName', data.user.flatTenant.TENANT_FIRST_NAME );
          this.router.navigate(['dashboard']);
          this.authService.isUserLoggedIn.next(true);
          this.presentToast('Login Sucessfull');
        } else if(data.user.security){
          localStorage.setItem('securityId', data.user.security.ID);
          localStorage.setItem('associationId', data.user.security.association.id);
          localStorage.setItem('appartmentId', data.user.security.association.appatmentInfo.id);
          localStorage.setItem('loginUserName', data.user.security.SECURITY_NAME);       
        //  this.router.navigate(['security/security-home']);
        this.router.navigate(['security/tabs']);
          this.authService.isUserLoggedIn.next(true);
        }
        else if (data.user.vendor) {
          if (data.user.vendor.association){
            localStorage.setItem("associationId", data.user.vendor.association.id);
          }
          if (data.user.vendor.vAssociation){
            localStorage.setItem("vAssociationId", data.user.vendor.vAssociation.id);
          }
          localStorage.setItem('vendorId', data.user.vendor.id);
          localStorage.setItem('loginUserName', data.user.vendor.VENDOR_NAME);
          this.router.navigate(['vdashboard']);
          this.authService.isUserLoggedIn.next(true);
          this.presentToast('Login Sucessfull');
        } else  {
          this.presentToast('Un Authoriszed Access');
        }
      
   
      } else {
        this.presentToast('In-Valid Login In Credentials');
      }
    });

  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
  onFormSubmit(formData: any): void {
    this.formData = formData;

    // post form data here
    alert(JSON.stringify(this.formData));
  }
  goToRegister() {
    this.navCtrl.navigateRoot('/register');
  }

  goToHome() {
    this.navCtrl.navigateRoot('/home-results');
  }

  goToRegistration() {
    // this.navCtrl.navigateRoot('/registration');
    this.router.navigate(['/registration']);
  }

  forgotPassword() {
    this.router.navigate(['/forgot-password']);
  }

  forgotUserName() {
    this.router.navigate(['/forgot-username']);
  }

}
