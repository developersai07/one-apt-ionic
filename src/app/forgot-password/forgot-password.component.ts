import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpCallsService } from '../common/http-service.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {

  public roleObj: any = [];

  form: FormGroup = new FormGroup({
    // username: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    role: new FormControl('', Validators.required),
  });
  constructor(public httpservice: HttpCallsService, public toastCtrl: ToastController, public router: Router) {
    this.getRoles();
  }

  ngOnInit() {}

  onSubmit(form) {
    const data = form.value;
    console.log(data);
    this.httpservice.addHttpPost('appUser/application/forgotPassword', data).subscribe((response: any) => {
      console.log(response.message);
      if (response.message == 'Success') {
        this.router.navigate(['']);
      } else {
        console.log(response.error);
        this.presentToast(response.message);
      }
    });
  }

  getRoles() {
    this.httpservice.getHttpPost('roles/getAll')
      .subscribe((data: any) => {
        this.roleObj = data;
        console.log(this.roleObj);
      });
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
