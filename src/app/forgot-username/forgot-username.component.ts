import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpCallsService } from '../common/http-service.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-username',
  templateUrl: './forgot-username.component.html',
  styleUrls: ['./forgot-username.component.scss'],
})
export class ForgotUsernameComponent implements OnInit {

  public roleObj: any = [];
  
  form: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    role: new FormControl('', Validators.required),
  });
  constructor(public httpservice: HttpCallsService, public toastCtrl: ToastController, public router: Router) {
    this.getRoles();
   }

  ngOnInit() {}

  getRoles() {
    this.httpservice.getHttpPost('roles/getAll')
      .subscribe((data: any) => {
        this.roleObj = data;
        console.log(this.roleObj);
      });
  }

  onSubmit(form) {
    const data = form.value;
    console.log(data);
    this.httpservice.addHttpPost('appUser/application/forgotUserName', data).subscribe((response: any) => {
      console.log(response.message);
      if (response.message == 'Success') {
        this.router.navigate(['']);
        this.presentToast(response.message);
      } else if (response.message == 'Check Your Email') {
        this.presentToast(response.message);
        console.log(response.error);
      }
    });
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
