import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public loginStatus: boolean;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public router: Router,
  ) {

  
  //  var appUserId =  localStorage.getItem("appUserId");
  //   if(appUserId){
  //     console.log("appUserId");
  //     this.loginStatus = true;
  //     this.router.navigate(['dashboard']);
  //   }
  //   else {
  //     this.loginStatus = false;
    
  //     this.router.navigate(['']);
  //   }
    this.initializeApp();

  }
  ionViewWillEnter() {

  }

  loadRoles(){
    console.log("load Roles In require way");
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
