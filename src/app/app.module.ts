import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationComponent } from '../app/components/registration/registration.component';
import { StepTwoComponent } from '../app/components/registration/step-two/step-two.component';
import { StepThreeComponent } from '../app/components/registration/step-three/step-three.component';
import { StepFourComponent } from '../app/components/registration/step-four/step-four.component';
import { AssociationRegistrationComponent } from '../app/components/registration/association-registration/association-registration.component';
import { AssociationSubRegistrationComponent } from '../app/components/registration/association-sub-registration/association-sub-registration.component';
import { VendorRegistrationComponent } from '../app/components/registration/vendor-registration/vendor-registration.component';
import { VendorSubRegistrationComponent } from '../app/components/registration/vendor-sub-registration/vendor-sub-registration.component';
import { AddRegistrationComponent } from './components/add-registration/add-registration.component';
import {PlansComponent} from './components/registration/plans/plans.component';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { DashboardComponent } from './components/menu/dashboard/dashboard.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { IndServiceSubComponent } from './components/ind-service-sub/ind-service-sub.component';
import {AuthService} from './providers/auth.service';
import {LogoutComponent} from './components/logout/logout.component';
import { HttpConfigInterceptor } from './interceptors/token.interceptor';
import { VendorDashboardComponent } from './components/vendor/vendor-dashboard/vendor-dashboard.component';
import { VendorIndTicketsComponent } from './components/vendor/vendor-ind-tickets/vendor-ind-tickets.component';
import { VendorComTicketsComponent } from './components/vendor/vendor-com-tickets/vendor-com-tickets.component';
import { IndTicketsComponent } from './components/association/ind-tickets/ind-tickets.component';
import { ComTicketsComponent } from './components/association/com-tickets/com-tickets.component';
import { UserFamilyComponent } from './components/family/user-family/user-family.component';
import { AddFamilyMembersModelPageModule } from './components/family/add-family-members-model/add-family-members-model.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ForgotUsernameComponent } from './forgot-username/forgot-username.component';
import { InternalVendorIndServceAmountComponent } from './components/internal-vendor-ind-servce-amount/internal-vendor-ind-servce-amount.component';
import { IndTicketAdditionalAmountComponent } from './components/ind-ticket-additional-amount/ind-ticket-additional-amount.component';
import { ComServiceSubComponent } from './components/com-service-sub/com-service-sub.component';
import { AssciationMenuComponent } from './components/association/assciation-menu/assciation-menu.component';
import { AssciationComTicketsComponent } from './components/association/assciation-com-tickets/assciation-com-tickets.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ChartsModule } from 'ng2-charts';
import {SuperTabsModule} from '@ionic-super-tabs/angular';
import { KidsExitComponent } from './components/security-management/kids-exit/kids-exit.component';
import { ParcelPickupComponent } from './components/security-management/parcel-pickup/parcel-pickup.component';

@NgModule({
  declarations: [AppComponent,
    RegistrationComponent,
    StepTwoComponent,
    StepThreeComponent,
    StepFourComponent,
    AssociationRegistrationComponent,
    AssociationSubRegistrationComponent,
    VendorRegistrationComponent,
    VendorSubRegistrationComponent,
    PlansComponent,
    AddRegistrationComponent,
    DashboardComponent,
    IndServiceSubComponent,
    SideMenuComponent,
    VendorDashboardComponent,
    VendorIndTicketsComponent,
    VendorComTicketsComponent,
    IndTicketsComponent,
    ComTicketsComponent,
    UserFamilyComponent,
    ForgotPasswordComponent,
    ForgotUsernameComponent,
    LogoutComponent,
    InternalVendorIndServceAmountComponent,
    IndTicketAdditionalAmountComponent,ComServiceSubComponent,
    AssciationMenuComponent,
    AssciationComTicketsComponent,
    ProfileComponent,
    KidsExitComponent,
    ParcelPickupComponent ],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, FormsModule, ReactiveFormsModule, RouterModule,
    HttpClientModule, AddFamilyMembersModelPageModule,
    ChartsModule,
    SuperTabsModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
