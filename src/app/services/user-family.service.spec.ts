import { TestBed } from '@angular/core/testing';

import { UserFamilyService } from './user-family.service';

describe('UserFamilyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserFamilyService = TestBed.get(UserFamilyService);
    expect(service).toBeTruthy();
  });
});
