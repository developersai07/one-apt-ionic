import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IndTicketAdditionalAmountService {
public insdTicketId: any;
public ticket: any;
public comTicketId: any;
  constructor() { }
  setIndTicketId(id){
this.insdTicketId = id;
  }
  setComTicketId(id){
    this.comTicketId = id;
  }
  getComTIcketId(){
    return this.comTicketId;
  }
  getIndTicketId(){
   return this.insdTicketId;
  }

  setTicket(tik){
 this.ticket = tik;
  }
  getTicket(){
    return this.ticket;
  }
}
