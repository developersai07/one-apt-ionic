import { TestBed } from '@angular/core/testing';

import { IndServiceService } from './ind-service.service';

describe('IndServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IndServiceService = TestBed.get(IndServiceService);
    expect(service).toBeTruthy();
  });
});
