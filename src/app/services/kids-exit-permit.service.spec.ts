import { TestBed } from '@angular/core/testing';

import { KidsExitPermitService } from './kids-exit-permit.service';

describe('KidsExitPermitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KidsExitPermitService = TestBed.get(KidsExitPermitService);
    expect(service).toBeTruthy();
  });
});
