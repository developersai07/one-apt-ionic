import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ParcelPickupService {

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    visitor_type: new FormControl('', Validators.required),
    company_name: new FormControl('', Validators.minLength(3)),
    guest_name: new FormControl('', Validators.minLength(3)),
    quantity: new FormControl('', Validators.required),
    pickup_note: new FormControl('', Validators.required)
  });

  constructor() { }
}
