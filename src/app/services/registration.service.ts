import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  form: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(2)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    mobile: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
  });
  formSteptwo: FormGroup = new FormGroup({
    address: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    pincode: new FormControl('', [Validators.required, Validators.minLength(6)]),
    enrolement: new FormControl('', Validators.required),
    landMark: new FormControl('', Validators.required),
  });
  formStepthreeAssc: FormGroup = new FormGroup({
    ass_registered: new FormControl('', Validators.required),
    app_registered: new FormControl('', Validators.required),
    apt_name: new FormControl('', Validators.required),
    ass_name: new FormControl('', Validators.required),
    no_of_flates_or_houses: new FormControl('', Validators.required)
  });
  associationPresident: FormGroup = new FormGroup({
    ass_president_name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    ass_president_email: new FormControl('', [Validators.required, Validators.email]),
    ass_president_mobile: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
    ass_president_address: new FormControl('', Validators.required),
    no_of_2_wheelers: new FormControl('', Validators.required),
    no_parking_slots: new FormControl('', Validators.required),
    no_of_4_wheelers: new FormControl('', Validators.required),
    ambulance_parking: new FormControl('', Validators.required),
    visitor_vehicle: new FormControl('', Validators.required)
  });
  associationSubRegistraion: FormGroup = new FormGroup({
    data_range: new FormControl('', Validators.required),
    no_of_report_cards: new FormControl('', Validators.required),
    no_of_flats_for_registration: new FormControl('', Validators.required),
    no_of_employee_login: new FormControl('', Validators.required),
    email_mode: new FormControl(true),
    sms_mode: new FormControl(true),
    whatsApp_mode: new FormControl(true),
    act_req_communication: new FormControl('', Validators.required),
    ass_payment: new FormControl('', Validators.required)
  });

  vendorRegistraion: FormGroup = new FormGroup({
    vendor_registered: new FormControl('', Validators.required),
    company_name: new FormControl('', Validators.required),
    emp_num: new FormControl('', Validators.required),
    num_clients_customers: new FormControl('', Validators.required),
  });
  vendorInfo: FormGroup = new FormGroup({
    vendor_name: new FormControl('', Validators.required),
    vendor_email: new FormControl('', Validators.required),
    vendor_mobile: new FormControl('', Validators.required),
    vendor_address: new FormControl('', Validators.required),
    //services_provided: new FormControl('',Validators.required),
    });
    vendorSubRegistraion: FormGroup = new FormGroup({
      data_range : new FormControl('',Validators.required),
      no_of_report_cards : new FormControl('',Validators.required),
      no_of_clients_customers_for_registration: new FormControl('',Validators.required),
      no_of_employee_login: new FormControl('',Validators.required),
      email_mode : new FormControl(''),
      sms_mode : new FormControl(''),
      whatsApp_mode: new FormControl(''),
      act_req_communication: new FormControl('',Validators.required),
      });
      plans: FormGroup = new FormGroup({
        plan : new FormControl('',Validators.required)
        });
        public appartmentRegistrationFile: any;
        public associationRegistrationFile : any;
        public  vendorRegistrationFile : any;
        
  constructor() { }

  populatenameSection(data) {

    console.log(data)
    this.form.controls['name'].setValue(data.name);
    this.form.controls['email'].setValue(data.email);
    this.form.controls['mobile'].setValue(data.mobile);
    // this.form.controls['address'].setValue(data.address);
    // this.form.controls['district'].setValue(data.district);
    // this.form.controls['state'].setValue(data.state);
    // this.form.controls['country'].setValue(data.country);
    // this.form.controls['pincode'].setValue(data.pincode);
    // this.form.controls['enrolement'].setValue(data.enrolement);
    // this.form.controls['registered'].setValue(data.registered);
    // this.form.controls['no_of_flates_or_houses'].setValue(data.no_of_flates_or_houses);
    // this.form.controls['company_name'].setValue(data.company_name);
    // this.form.controls['emp_num'].setValue(data.emp_num);
    // this.form.controls['num_clients_customers'].setValue(data.num_clients_customers);
    // this.form.controls['reg_certificate'].setValue(data.reg_certificate);
    // this.form.controls['ass_president_name'].setValue(data.ass_president_name);
    // this.form.controls['ass_president_email'].setValue(data.ass_president_email);
    // this.form.controls['ass_president_mobile'].setValue(data.ass_president_mobile);
    // this.form.controls['ass_president_address'].setValue(data.ass_president_address);
    // this.form.controls['no_of_2_wheelers'].setValue(data.no_of_2_wheelers);
    // this.form.controls['no_of_4_wheelers'].setValue(data.no_of_4_wheelers);
    // this.form.controls['ambulance_parking'].setValue(data.ambulance_parking);
    // this.form.controls['common_area_service'].setValue(data.common_area_service);
    // this.form.controls['visitor_vehicle'].setValue(data.visitor_vehicle);
  }

  serviceAppartmentSetRegistratinFile(appartmentRegistraionFile){
this.appartmentRegistrationFile = appartmentRegistraionFile;
  }
  getserviceAppartmentSetRegistratinFile(){
    return this.appartmentRegistrationFile; 
  }
  serviceAssociatinSetRegistratinFile(associationRegistraionFile){
    this.associationRegistrationFile = associationRegistraionFile;
      }
      getserviceAssociationSetRegistratinFile(){
        return this.associationRegistrationFile; 
      }


      serviceVendorSetRegistratinFile(vendorRegistrationFile){
        this.vendorRegistrationFile = vendorRegistrationFile;
      }
      getserviceVendorSetRegistratinFile(){
        return this.vendorRegistrationFile; 
      }
}
