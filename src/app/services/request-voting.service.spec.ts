import { TestBed } from '@angular/core/testing';

import { RequestVotingService } from './request-voting.service';

describe('RequestVotingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestVotingService = TestBed.get(RequestVotingService);
    expect(service).toBeTruthy();
  });
});
