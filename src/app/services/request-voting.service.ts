import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class RequestVotingService {

  requestVotingForm: FormGroup = new FormGroup({
    id: new FormControl(null),
    title: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  });
  
  ticketId: any;

  constructor() { }

  setTicketId(id) {
    this.ticketId = id;
  }
  getTicketId() {
    return this.ticketId;
  }
}
