import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
@Injectable({
  providedIn: 'root'
})
export class ComAreaTIcketService {
  form: FormGroup = new FormGroup({
    ticket_description: new FormControl('', Validators.required),
    ticket_date: new FormControl('', Validators.required),
    ticket_time: new FormControl('', Validators.required),
    service_subscription_id: new FormControl('', Validators.required),
    ind_service_id: new FormControl('', Validators.required),
    ticket_img: new FormControl('')
  });
  public serviceiD

  constructor() { }

  setServiceId(id) {
    this.serviceiD = id;
  }
  getServiceiD() {
    return this.serviceiD;
  }

  initializeFormGroup(serviceTypeId, quantity) {

    if (serviceTypeId && quantity) {
      this.form.setValue({
        ticket_description: '',
        ticket_date: '',
        ticket_time: '',
        service_subscription_id: '',
        ind_service_id : '',
        ticket_img: '',
        service_types_id : '',
        quantity : ''
      });
    } else if (serviceTypeId) {
      this.form.setValue({
        ticket_description: '',
        ticket_date: '',
        ticket_time: '',
        service_subscription_id: '',
        ind_service_id : '',
        ticket_img: '',
        service_types_id : '',
      //  quantity : ''
      });
    } else if (quantity) {
      this.form.setValue({
        ticket_description: '',
        ticket_date: '',
        ticket_time: '',
        service_subscription_id: '',
        ind_service_id : '',
        ticket_img: '',
        quantity : ''
      });
    } else {
      this.form.setValue({
        ticket_description: '',
        ticket_date: '',
        ticket_time: '',
        service_subscription_id: '',
        ind_service_id : '',
        ticket_img: ''
      });
    }
  }

}
