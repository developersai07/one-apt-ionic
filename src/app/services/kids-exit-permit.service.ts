import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class KidsExitPermitService {

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    // kid_name: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
    // kid_age: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]')])),
    relation_id: new FormControl('', Validators.required),
    guardian_name: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
    guardian_mobile_number: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]{10}')])),
    permit_date: new FormControl('', Validators.required),
    permit_time: new FormControl('', Validators.required)
  });

  constructor() { }
}
