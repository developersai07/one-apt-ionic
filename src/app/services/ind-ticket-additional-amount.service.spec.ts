import { TestBed } from '@angular/core/testing';

import { IndTicketAdditionalAmountService } from './ind-ticket-additional-amount.service';

describe('IndTicketAdditionalAmountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IndTicketAdditionalAmountService = TestBed.get(IndTicketAdditionalAmountService);
    expect(service).toBeTruthy();
  });
});
