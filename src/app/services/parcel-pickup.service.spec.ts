import { TestBed } from '@angular/core/testing';

import { ParcelPickupService } from './parcel-pickup.service';

describe('ParcelPickupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParcelPickupService = TestBed.get(ParcelPickupService);
    expect(service).toBeTruthy();
  });
});
