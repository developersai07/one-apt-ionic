import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
@Injectable({
  providedIn: 'root'
})
export class IndServiceService {
  form: FormGroup = new FormGroup({
    ticket_description: new FormControl('', Validators.required),
    ticket_date: new FormControl('', Validators.required),
    ticket_time: new FormControl('', Validators.required),
    service_subscription_id: new FormControl('',Validators.required),
    ticket_img: new FormControl('')
  });
  public indServiceId;

  constructor() { }
  setServiceId(indServiceId){
this.indServiceId = indServiceId
  }
  getServiceId(){
    return this.indServiceId;
  }

  initializeFormGroup(serviceTypeId,quantity){

    console.log(serviceTypeId)

    console.log(quantity)

    if(serviceTypeId && quantity){
      this.form.setValue({
        ticket_description: '',
        ticket_date: '',
        ticket_time: '',
        service_subscription_id: '',
        ticket_img: '',
        service_types_id : '',
        quantity : ''
      });
    }
else if(serviceTypeId){

  console.log("serviceTyepIdExist")
  this.form.setValue({
    ticket_description: '',
    ticket_date: '',
    ticket_time: '',
    service_subscription_id: '',
    ticket_img: '',
    service_types_id : '',
  });
    }
    else if(quantity){
      this.form.setValue({
        ticket_description: '',
        ticket_date: '',
        ticket_time: '',
        service_subscription_id: '',
        ticket_img: '',
        quantity : ''
      });
        }
        else {
          this.form.setValue({
            ticket_description: '',
            ticket_date: '',
            ticket_time: '',
            service_subscription_id: '',
            ticket_img: ''
          }); 
        }
  
  }


}
