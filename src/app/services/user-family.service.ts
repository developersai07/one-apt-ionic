import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UserFamilyService {
  userRelativeForm: FormGroup = new FormGroup({
    id: new FormControl(null),
    relative_name: new FormControl('', [Validators.required, Validators.minLength(2)]),
    phone_number: new FormControl('', [Validators.required, Validators.minLength(10)]),
    email_id: new FormControl('', [Validators.required, Validators.email]),
    age: new FormControl('', Validators.required),
    relation_id: new FormControl('', Validators.required),
    // app_user_id: new FormControl('', Validators.required),
    // association_id: new FormControl('', Validators.required),
  });
  
  constructor() { }
  
  emptyFormGroup() {
    this.userRelativeForm.setValue({
      id: null,
      relative_name: '',
      phone_number: '',
      email_id: '',
      age: '',
      relation_id: '',
    });
  }

  editForm(relation){
    this.userRelativeForm.controls['id'].setValue(relation.ID);
    this.userRelativeForm.controls['relative_name'].setValue(relation.RELATIVE_NAME);
    this.userRelativeForm.controls['phone_number'].setValue(relation.PHONE_NUMBER);
    this.userRelativeForm.controls['email_id'].setValue(relation.EMAIL_ID);
    this.userRelativeForm.controls['age'].setValue(relation.AGE);
    this.userRelativeForm.controls['relation_id'].setValue(relation.familyRelationsEntity.ID);
  }

}
