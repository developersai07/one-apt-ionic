import { TestBed } from '@angular/core/testing';

import { ComAreaTIcketService } from './com-area-ticket.service';

describe('ComAreaTIcketService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComAreaTIcketService = TestBed.get(ComAreaTIcketService);
    expect(service).toBeTruthy();
  });
});
