import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  public ImageName: any;

  profileForm: FormGroup = new FormGroup({
    id: new FormControl(null),
    first_name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    last_name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    mobile_number: new FormControl('', [Validators.required, Validators.minLength(10)]),
    owner_img : new FormControl(''),
    // address: new FormControl('', Validators.required),
    // profile_pic: new FormControl(''),
   });

  vendorProfileForm: FormGroup = new FormGroup({
    id: new FormControl(null),
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    mobile_number: new FormControl('', [Validators.required, Validators.minLength(10)]),
    // address: new FormControl('', Validators.required),
    owner_img : new FormControl(''),
  });

  populateForm(data) {
    this.profileForm.controls['id'].setValue(data.FLAT_OWNER_ID);
    this.profileForm.controls['first_name'].setValue(data.OWNER_FIRST_NAME);
    this.profileForm.controls['last_name'].setValue(data.OWNER_LAST_NAME);
    this.profileForm.controls['email'].setValue(data.OWNER_EMAIL);
    this.profileForm.controls['mobile_number'].setValue(data.OWNER_MOBILE_NUMBER);
    this.setuser_IMAGE_NAME(data.OWNER_PIC);
  }

  populateTenantForm(data) {
    // console.log('----------------');
    // console.log(data);
    this.profileForm.controls['id'].setValue(data.TENANT_ID);
    this.profileForm.controls['first_name'].setValue(data.TENANT_FIRST_NAME);
    this.profileForm.controls['last_name'].setValue(data.TENANT_LAST_NAME);
    this.profileForm.controls['email'].setValue(data.TENANT_EMAIL);
    this.profileForm.controls['mobile_number'].setValue(data.TENANT_MOBILE_NUMBER);
    // this.profileForm.controls['address'].setValue(data.TENANT_ADDRESS);
    this.setuser_IMAGE_NAME(data.TENANT_PIC);
  }

  populateVendorForm(data) {
    console.log('----------------');
    console.log(data);
    console.log(data.VENDOR_NAME);
    this.vendorProfileForm.controls['id'].setValue(data.id);
    this.vendorProfileForm.controls['name'].setValue(data.VENDOR_NAME);
    this.vendorProfileForm.controls['email'].setValue(data.VENDOR_EMAIL);
    this.vendorProfileForm.controls['mobile_number'].setValue(data.VENDOR_MOBILE_NUMBER);
    // this.vendorProfileForm.controls['address'].setValue(data.VENDOR_ADDRESS);
    this.setuser_IMAGE_NAME(data.VENDOR_PIC);
  }

  

  constructor() { }

  setuser_IMAGE_NAME(imageName) {

    console.log("this.imageName")
    console.log(imageName);
    this.ImageName = imageName;
  }

  getuser_IMAGE_NAME() {
    console.log(this.ImageName);
    return this.ImageName;
  }
}
