//httpConfig.interceptor.ts
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
  } from '@angular/common/http';
  import { Observable, throwError } from 'rxjs';
  import { map, catchError } from 'rxjs/operators';
  import { Injectable } from '@angular/core';
  import { ToastController } from '@ionic/angular';
  import { Router, ActivatedRoute } from '@angular/router';
   
  @Injectable()
  export class HttpConfigInterceptor implements HttpInterceptor {
    
    constructor(  public toastCtrl: ToastController,public router :Router) { }
   
   
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


      const jwt = localStorage.getItem("jwt");
      if (jwt) {
          request = request.clone({
              setHeaders: { 
                  Authorization: `Bearer ${jwt}`
              }
          });
      }

     // return next.handle(request);
  
      return next.handle(request).pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            console.log('event--->>>', event);
          }
          return event;
        }),
        catchError((error: HttpErrorResponse) => {
          console.error(error);
          if (error.status === 401) 
          {
            this.presentToast('Session Expired. Please re-login');
            localStorage.clear();
      
            this.router.navigate(['']);
        
           
          }
          return throwError(error);
        }));
    }
    async presentToast(msg) {
        const toast = await this.toastCtrl.create({
          message: msg,
          duration: 2000
        });
        toast.present();
      }
   
  }
   