import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { RegistrationComponent } from './components/registration/registration.component';
import { StepTwoComponent } from './components/registration/step-two/step-two.component';
import { StepThreeComponent } from './components/registration/step-three/step-three.component';
import { StepFourComponent } from './components/registration/step-four/step-four.component';
import { AssociationRegistrationComponent } from './components/registration/association-registration/association-registration.component';
import { AssociationSubRegistrationComponent } from './components/registration/association-sub-registration/association-sub-registration.component';
import { VendorRegistrationComponent } from './components/registration/vendor-registration/vendor-registration.component';
import { VendorSubRegistrationComponent } from './components/registration/vendor-sub-registration/vendor-sub-registration.component';
import { AddRegistrationComponent } from './components/add-registration/add-registration.component';
import { PlansComponent } from './components/registration/plans/plans.component';
import { DashboardComponent } from './components/menu/dashboard/dashboard.component';
import { IndServiceSubComponent } from './components/ind-service-sub/ind-service-sub.component';
import { LogoutComponent } from './components/logout/logout.component';
import { VendorDashboardComponent } from './components/vendor/vendor-dashboard/vendor-dashboard.component';
import { VendorIndTicketsComponent } from './components/vendor/vendor-ind-tickets/vendor-ind-tickets.component';
import { VendorComTicketsComponent } from './components/vendor/vendor-com-tickets/vendor-com-tickets.component';
import { IndTicketsComponent } from './components/association/ind-tickets/ind-tickets.component';
import { ComTicketsComponent } from './components/association/com-tickets/com-tickets.component';
import {  UserFamilyComponent } from './components/family/user-family/user-family.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ForgotUsernameComponent } from './forgot-username/forgot-username.component';

import { InternalVendorIndServceAmountComponent } from './components/internal-vendor-ind-servce-amount/internal-vendor-ind-servce-amount.component';
import { IndTicketAdditionalAmountComponent } from './components/ind-ticket-additional-amount/ind-ticket-additional-amount.component';
import { ComServiceSubComponent } from './components/com-service-sub/com-service-sub.component';
import { AssciationMenuComponent } from './components/association/assciation-menu/assciation-menu.component';
import { AssciationComTicketsComponent } from './components/association/assciation-com-tickets/assciation-com-tickets.component';
import { ProfileComponent } from './components/profile/profile.component';
import { KidsExitComponent } from './components/security-management/kids-exit/kids-exit.component';
import { ParcelPickupComponent } from './components/security-management/parcel-pickup/parcel-pickup.component';

const routes: Routes = [
  { path: 'home', redirectTo: 'home', pathMatch: 'full' },
  { path: '', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'forgot-password', component: ForgotPasswordComponent, pathMatch: 'full'} ,
  { path: 'forgot-username', component: ForgotUsernameComponent, pathMatch: 'full'} ,
  { path: 'registration', component: RegistrationComponent, pathMatch: 'full' },
  { path: 'step-two', component: StepTwoComponent, pathMatch: 'full' },
  { path: 'step-three', component: StepThreeComponent, pathMatch: 'full' },
  { path: 'step-four', component: StepFourComponent, pathMatch: 'full' },
  { path: 'association-registration', component: AssociationRegistrationComponent, pathMatch: 'full' },
  { path: 'association-sub-registration', component: AssociationSubRegistrationComponent, pathMatch: 'full' },
  { path: 'vendor-registration', component: VendorRegistrationComponent, pathMatch: 'full' },
  { path: 'vendor-sub-registration', component: VendorSubRegistrationComponent, pathMatch: 'full' },
  { path: 'registerAddress', component: AddRegistrationComponent, pathMatch: 'full' },
  { path: 'indServiceSub', component: IndServiceSubComponent, pathMatch: 'full' },
  { path: 'plans', component: PlansComponent, pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent, pathMatch: 'full' },
  { path: 'vdashboard', component: VendorDashboardComponent, pathMatch: 'full' },
  { path: 'vendorIndividualTickets', component: VendorIndTicketsComponent, pathMatch: 'full' },
  { path: 'vendorCommonTickets', component: VendorComTicketsComponent, pathMatch: 'full' },
  { path: 'assIndividualTickets', component: IndTicketsComponent, pathMatch: 'full' },
  { path: 'assCommonTickets', component: ComTicketsComponent, pathMatch: 'full' },
  { path: 'familyDetails', component: UserFamilyComponent, pathMatch: 'full'},
  { path: 'logout', component: LogoutComponent, pathMatch: 'full' },
  {
    path: 'add-family-members-model',
    loadChildren: () => import('./components/family/add-family-members-model/add-family-members-model.module').then( m => m.AddFamilyMembersModelPageModule)
  },
  {
    path: 'security', loadChildren: './modules/security/security.module#SecurityModule'
  },
  {
    path: 'visitor', loadChildren: './modules/Owner-Visitors/visitor/visitor.module#VisitorModule'
  },
  { path: 'indServiceManage', component: InternalVendorIndServceAmountComponent, pathMatch: 'full' },
  { path: 'indAddtionalTicketAmount', component: IndTicketAdditionalAmountComponent, pathMatch: 'full' },
  { path: 'comServiceRaise', component: ComServiceSubComponent, pathMatch: 'full' },
  { path: 'associationMenu', component: AssciationMenuComponent, pathMatch: 'full' },
  { path: 'associationComTickets', component: AssciationComTicketsComponent, pathMatch: 'full' },
  { path: 'kidsExitPermit', component: KidsExitComponent, pathMatch: 'full'},
  { path: 'parcelPickup', component: ParcelPickupComponent, pathMatch: 'full'},
  { path: 'profile', component: ProfileComponent, pathMatch: 'full' },

  { path: 'voting', loadChildren: './modules/voting/voting.module#VotingModule'},
  
  
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
